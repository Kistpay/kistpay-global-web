export function RevealAnimated() {
    let reveals = document.querySelectorAll(".reveal");

    for (let i = 0; i < reveals.length; i++) {
        let windowHeight = window.innerHeight;
        let elementTop = reveals[i].getBoundingClientRect().top;
        let elementVisible = 150;

        if (elementTop < windowHeight - elementVisible) {
            reveals[i].classList.add("active");
            reveals[i].classList.add("fadeInDown");
        }
        // else {
        //     reveals[i].classList.remove("active");
        //     reveals[i].classList.remove("fadeInDown");
        // }
    }
}

export function RevealAnimatedFadeIn() {
    let reveals = document.querySelectorAll(".reveal-fade-in");

    for (let i = 0; i < reveals.length; i++) {
        let windowHeight = window.innerHeight;
        let elementTop = reveals[i].getBoundingClientRect().top;
        let elementVisible = 150;

        if (elementTop < windowHeight - elementVisible) {
            reveals[i].classList.add("active");
            reveals[i].classList.add("fadeIn");
        } else {
            reveals[i].classList.remove("active");
            reveals[i].classList.remove("fadeIn");
        }
    }
}

export function RevealAnimatedRotateFromLeft() {
    let reveals = document.querySelectorAll(".reveal-lft");

    for (let i = 0; i < reveals.length; i++) {
        let windowHeight = window.innerHeight;
        let elementTop = reveals[i].getBoundingClientRect().top;
        let elementVisible = 150;

        if (elementTop < windowHeight - elementVisible) {
            reveals[i].classList.add("active");
            reveals[i].classList.add("fadeInLeft");
        } else {
            reveals[i].classList.remove("active");
            reveals[i].classList.remove("fadeInLeft");
        }
    }
}

export function showHeaderBackground() {
    let headerHeight = document.getElementsByClassName("contentStart");
    let headerHld = document.getElementsByClassName("kp-header-main");
    let headerTxtLink = document.getElementsByClassName("link");
    let windowHeight = window.innerHeight;
    let elementTop = headerHeight[0].getBoundingClientRect().top;
    let elementVisible = 400;
    if (elementTop < windowHeight - elementVisible) {
        headerHld[0].classList.add("active");
        headerTxtLink[0].classList.add("active");
    } else {
        headerHld[0].classList.remove("active");
        headerTxtLink[0].classList.remove("active");
    }
}

export function toggleScrollIcon() {
    let targetHeight = document.getElementsByClassName("down-arrow");
    let windowHeight = window.innerHeight;
    let elementTop = targetHeight[0].getBoundingClientRect().top;
    let elementVisible = 150;
    if (elementTop < windowHeight - elementVisible) {
        targetHeight[0].classList.add("dbl");
    } else {
        targetHeight[0].classList.remove("dbl");
    }
}

export function RevealAnimatedRotateFromRight() {
    let reveals = document.querySelectorAll(".reveal-rt");

    for (let i = 0; i < reveals.length; i++) {
        let windowHeight = window.innerHeight;
        let elementTop = reveals[i].getBoundingClientRect().top;
        let elementVisible = 150;

        if (elementTop < windowHeight - elementVisible) {
            reveals[i].classList.add("active");
            reveals[i].classList.add("fadeInRight");
        } else {
            reveals[i].classList.remove("active");
            reveals[i].classList.remove("fadeInRight");
        }
    }
}

// export function animation3DEffect(){
//     let aElem = document.getElementsByClassName('Animation3DEffect');
//     const docStyle = document.documentElement.style
//     const boundingClientRect = aElem[0].getBoundingClientRect()
//
//     aElem.onmousemove = function(e) {
//
//         const x = e.clientX - boundingClientRect.left
//         const y = e.clientY - boundingClientRect.top
//
//         const xc = boundingClientRect.width/2
//         const yc = boundingClientRect.height/2
//
//         const dx = x - xc
//         const dy = y - yc
//
//         docStyle.setProperty('--rx', `${ dy/-1 }deg`)
//         docStyle.setProperty('--ry', `${ dx/10 }deg`)
//
//     }
//
//     aElem.onmouseleave = function(e) {
//
//         docStyle.setProperty('--ty', '0')
//         docStyle.setProperty('--rx', '0')
//         docStyle.setProperty('--ry', '0')
//
//     }
//
//     aElem.onmousedown = function(e) {
//
//         docStyle.setProperty('--tz', '-25px')
//
//     }
//
//     document.body.onmouseup = function(e) {
//
//         docStyle.setProperty('--tz', '-12px')
//
//     }
// }

