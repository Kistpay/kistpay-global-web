import axios from "axios";

// const BASE_URL = `http://192.168.80.28:443`;
// const BASE_URL = `http://192.168.80.201:443`;
// const BASE_URL = `http://13.214.151.53:8080`;
// const BASE_URL = `https://15.185.32.204:443`;
const BASE_URL = `https://retailer.kistpay.com`;

export const API = axios.create({
    baseURL: BASE_URL,
    timeout: 60000
});