const DOMAIN_PREFIX = "";

export const Enums = {
    THEME: {
        PRIMARY_COLOR: "#2d67e1",
    },
    ROUTES: {
        DEFAULT: `${DOMAIN_PREFIX}/`,
        FOR_CUSTOMER: `${DOMAIN_PREFIX}/ForCustomer`,
        FOR_RETAILER: `${DOMAIN_PREFIX}/ForRetailer`,
        COMPANY: `${DOMAIN_PREFIX}/Company`,
        CAREER: `${DOMAIN_PREFIX}/Career`,
        TC: `${DOMAIN_PREFIX}/TermsAndCondition`,
        PP: `${DOMAIN_PREFIX}/PrivacyPolicy`,
    }
}