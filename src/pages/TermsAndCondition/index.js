import React from 'react';
import "../Landing/index.css";
import "../TermsAndCondition/index.css";
import Header from "../../SharedComponents/Header";
import SectionFive from "../Landing/Sections/Five";
import Footer from "../../SharedComponents/Footer";
import SectionOneTermsAndCondition from "./Sections/one";

const TermsAndConditions = () => {
    return (
        <div className="main">
            <Header/>

            <SectionOneTermsAndCondition/>

            {/*this is Section from landing dir*/}
            <SectionFive/>
            <Footer/>
        </div>
    );
};

export default TermsAndConditions;