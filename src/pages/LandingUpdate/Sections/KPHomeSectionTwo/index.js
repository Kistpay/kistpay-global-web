import React from 'react';
import "./KPHomeSectionTwo.css";
// import _SecTwoImg from "../../../../images/sections/two/section_two_bg.png";
import _SecTwoImg from "../../../../images/sections/two/cover_sec_two1.png";
import {Animate} from 'react-simple-animate';
import {Parallax} from "react-scroll-parallax";

const KPHomeSectionTwo = ({bg}) => {
    return (
        <div className={`cont-main bg_sec sec_1_mob contentStart`} style={{
            backgroundColor: bg !== undefined && bg
        }}>
            <div className={`section`} style={{backgroundColor: "inherit"}}>
                <div className="ctr-sec">
                    <div className="img-hld">

                        <div className="_bg" style={{
                            backgroundImage: `url(${_SecTwoImg})`
                        }}/>
                        {/*<Animate*/}
                        {/*    start={{transform: 'translate(200px, 0)'}}*/}
                        {/*    end={{transform: 'translate(0px, 20px)'}}*/}
                        {/*    play>*/}
                        {/*    */}
                        {/*<img className="_bg" alt={"#"} src={_SecTwoImg}/>*/}
                        {/*</Animate>*/}
                    </div>
                    <Parallax
                        translateX={["-70px", "0px"]}
                        scale={[1, 1]}
                        // rotate={[-180, 0]}
                        easing="easeInQuad"
                    >
                        <div className="typo-sec">
                            <div className="txt-pri">OUR</div>
                            <div className="txt-sec">VISION</div>
                        </div>
                    </Parallax>
                    <Parallax
                        translateX={["70px", "0px"]}
                        scale={[1, 1]}
                        // rotate={[-180, 0]}
                        easing="easeInQuad"
                    >
                        <Animate
                            start={{transform: 'translate(-200px, 0)'}}
                            end={{transform: 'translate(0px, 0px)'}}
                            play>

                            <div className="card-sec">
                                <div className="card-point">
                                    <div className="p">Kistpay's belief is that everyone deserves the benefits of
                                        a <strong className="st">digital connected life.</strong></div>
                                    <br/>

                                    <div className="p">With our Shariah Compliant financing model, we aim to
                                        make <strong className="st">access to quality Smartphones</strong> as easy as
                                        possible for everyone.
                                    </div>
                                    <br/>

                                    <div className="p">It is our firm belief that providing Smartphone will give access
                                        to information which will reduce poverty, simultaneously opening up new
                                        information channels by <strong className="st">educating & upskilling the youth,</strong> resulting in creation of micro-entrepreneurs & transformed
                                        communities.
                                    </div>
                                    <br/>
                                    {/*Kistpay is enabling the next billion users to live a digital connected life. Through our Digital Connected Life mission, 1 billion people will be enabled to use their smartphones to connect to the digital economy. " This programme is also accelerating the drive towards financial inclusion and women's empowerment, where those previously considered as ‘unbanked’ are able to use the new infrastructure to make financial transactions and build their credit history.*/}

                                    {/*KistPay's belief is that everyone deserves the benefits of a digitally connected life. Through increasing access to smartphones, we can help communities transform. Access to information can reduce poverty by educating and upskilling youth, creating jobs and improving lives.*/}
                                </div>
                            </div>
                        </Animate>
                    </Parallax>
                </div>
            </div>
        </div>
    );
};

export default KPHomeSectionTwo;