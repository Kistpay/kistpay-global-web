import React, {useEffect} from 'react';
import "./KPHomeSectionFive.css";
import checkIc from "../../../../images/sections/five/check1_.png";
import cvBgIc from "../../../../images/sections/five/bg_five1.png";
// import {RevealAnimated, RevealAnimatedFadeIn} from "../../../../utils/DefaultFunctions";

const KPHomeSectionFive = ({bg}) => {

    useEffect(() => {
        // window.addEventListener("scroll", RevealAnimated);
        // window.addEventListener("scroll", RevealAnimatedFadeIn);
    }, [])

    return (<div className={`cont-main bg_sec sec_1_mob`} style={{
        backgroundColor: bg !== undefined && bg
    }}>
        <div className={`section`} style={{backgroundColor: "inherit"}}>
            <div className="cnt-sec-five">
                <div className="top">
                    <div className="hdr">
                        <div className="txt-p">
                            <div className="ic animate reveal-fade-in">
                                <img alt={"#"} src={checkIc}/>
                            </div>
                            <div className="p">
                                Same Day Processing
                            </div>
                        </div>
                        <div className="txt-p">
                            <div className="ic animate reveal-fade-in">
                                <img alt={"#"} src={checkIc}/>
                            </div>
                            <div className="p">
                                Shariah Compliant
                            </div>
                        </div>
                        <div className="txt-p">
                            <div className="ic animate reveal-fade-in">
                                <img alt={"#"} src={checkIc}/>
                            </div>
                            <div className="p">
                                Takaful Islamic Insurance
                            </div>
                        </div>
                        <div className="txt-p">
                            <div className="ic animate reveal-fade-in">
                                <img alt={"#"} src={checkIc}/>
                            </div>
                            <div className="p">
                                Smart Installment Plans
                            </div>
                        </div>
                        {/*<div className="txt-p">*/}
                        {/*    <div className="ic animate reveal-fade-in">*/}
                        {/*        <img alt={"#"} src={checkIc}/>*/}
                        {/*    </div>*/}
                        {/*    <div className="p">*/}
                        {/*        Free Data Plans*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        <div className="txt-p">
                            <div className="ic animate reveal-fade-in">
                                <img alt={"#"} src={checkIc}/>
                            </div>
                            <div className="p">
                                Smartphone Anti-theft
                            </div>
                        </div>
                    </div>
                </div>
                <div className="center">
                    <div className="cv-bg">
                        <img alt="#" src={cvBgIc}/>
                        <div className="clr-op"/>
                    </div>
                    <div className="crd-sec animate reveal">
                        <div className="crd">
                            <div className="head">
                                Our Story
                            </div>
                            <div className="p">
                                <div className="gp">
                                    Kistpay was formed by Asif Jafri and Amir Jafri, who are also the founders of
                                    e.ocean,
                                    Pakistan’s largest Digital Communications Solution Provider,
                                </div>
                                <div className="gp">
                                    Their mission is to bring the next billion users online by providing smartphone
                                    access
                                    through a Shariah Compliant BNPL financing model.
                                </div>
                                <div className="gp">
                                    Kistpay is also the only technology company of its sort in the MENAP region that
                                    offers
                                    integrated proprietary MDM Technology.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*<div className="btm"/>*/}
            </div>
        </div>
    </div>);
};

export default KPHomeSectionFive;