import React, {useState} from 'react';
import "./KPHomeSectionSeven.css";
// import secSevImg from "../../../../images/sections/seven/secSevImg.png";
import secSevImg from "../../../../images/sections/seven/secSevImg3.png";
import secSevBg from "../../../../images/bg_sec_seven_1.png";


const KPHomeSectionSeven = ({bg}) => {
    return (
        <div className={`cont-main bg_sec sec_1_mob`} style={{
            backgroundColor: bg !== undefined && bg,
            backgroundImage: `url(${secSevBg})`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
        }}>
            <div className={`section`} style={{backgroundColor: "transparent"}}>
                <div className="cnt-sec-seven">
                    <div className="top">
                        <div className="heading">
                            <div className="txt">
                                INDUSTRIES WE ARE CONNECTED WITH
                            </div>
                        </div>
                    </div>
                    <div className="center">
                        <div className="img-hld">
                            <img alt={"#"} src={secSevImg}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default KPHomeSectionSeven;