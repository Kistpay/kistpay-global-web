import React, {useEffect, useState} from 'react';
import "./KPHomeSectionEight.css";
import image1 from "../../../../images/sections/eight/k1.jpg";
import image3 from "../../../../images/sections/eight/k3.jpg";
import image4 from "../../../../images/sections/eight/k4.jpg";
import image5 from "../../../../images/sections/eight/k5.jpg";
import image8 from "../../../../images/sections/eight/k8.jpg";
import image9 from "../../../../images/sections/eight/k9.jpg";
import image10 from "../../../../images/sections/eight/k10.jpg";
import image11 from "../../../../images/sections/eight/k11.jpg";
// import image2 from "../../../../images/sections/eight/k2.jpg";
// import image6 from "../../../../images/sections/eight/k6.jpg";
// import image7 from "../../../../images/sections/eight/k7.jpg";
// import arrow_btn_lft from "../../../../images/arrow_btn_lft.png";
// import arrow_btn_rt from "../../../../images/arrow_btn_rt.png";
// import {ToggleCard} from "react-stack-cards";
//import Swiper from "swiper";
// import "swiper/css";
// import {RevealAnimated} from "../../../../utils/DefaultFunctions";
// import {Carousel} from '3d-react-carousal';
import _3DSliderReact from "../../../../SharedComponents/ThreeDReactSlider";
// import 'swiper/css/navigation';
// import 'swiper/css/pagination';

const KPHomeSectionEight = ({bg}) => {
    const [init, setInit] = useState({
        directionToggle: "sideSlide",
        images: [image8, image3, image1, image4, image5, image9, image10, image11],
        isOpen: false,
        sliderAutoPlay: true
    });

    let {images} = init;
    let isBrowser = typeof window !== 'undefined';

    useEffect(() => {
        console.log("isBrowser >>>", isBrowser)
        if (isBrowser) {
            setInit({
                ...init,
                isOpen: true
            })
        }
    }, [isBrowser])

    // useEffect(() => {
    //     if (images) {
    //         setInit({
    //             ...init,
    //             isOpen: true
    //         })
    //     }
    //
    // }, [images]);


    // const SwiperSlider = (imagesArr) => {
    //
    //
    //
    //     const swiper = new Swiper('.swiper', {
    //         // Optional parameters
    //         direction: 'horizontal',
    //         loop: true,
    //
    //         // If we need pagination
    //         pagination: {
    //             el: '.swiper-pagination',
    //         },
    //
    //         // Navigation arrows
    //         navigation: {
    //             nextEl: '.swiper-button-next',
    //             prevEl: '.swiper-button-prev',
    //         },
    //
    //         // And if we need scrollbar
    //         scrollbar: {
    //             el: '.swiper-scrollbar',
    //         },
    //     });
    //     return (
    //         <div className="swiper" style={{
    //             width: "600px",
    //             height: "300px"
    //         }}>
    //             {/*// <!-- Additional required wrapper -->*/}
    //             <div className="swiper-wrapper">
    //                 {/*// <!-- Slides -->*/}
    //                 {
    //                     imagesArr.map((im) => {
    //                         return (
    //                             <div className="swiper-slide">
    //                                 <img style={{
    //                                     width: "100%",
    //                                     height: "300px",
    //                                     borderRadius: "1rem"
    //                                 }} alt={"#"} src={im}/>
    //                             </div>
    //                         )
    //                     })
    //                 }
    //             </div>
    //             {/*// <!-- If we need pagination -->*/}
    //             <div className="swiper-pagination"/>
    //
    //             {/*// <!-- If we need navigation buttons -->*/}
    //             <div className="swiper-button-prev"/>
    //             <div className="swiper-button-next"/>
    //
    //             {/*// <!-- If we need scrollbar -->*/}
    //             <div className="swiper-scrollbar"/>
    //         </div>
    //     )
    // }

    // const callback = (im) => {
    //     console.log("callback >>>", im)
    // }


    //<!--prev-updated slider--->

    // const SwiperSlider = (imagesArr, isOpen) => {
    //     return (
    //         <div className="swiper" style={{
    //             width: "60rem",
    //             // height: "300px"
    //         }}>
    //             {
    //                 isOpen && (
    //                     <Carousel slides={imagesArr.map((im) => {
    //                         return (
    //                             <img style={{
    //                                 width: "100%",
    //                                 height: "300px",
    //                                 borderRadius: "1rem"
    //                             }} alt={"#"} src={im}/>
    //                         )
    //                     })}
    //                               autoplay={true}
    //                               arrows={false}
    //                               interval={3000}
    //                         // onSlideChange={callback}
    //                     />
    //                 )
    //             }
    //         </div>
    //     )
    // }

    return (
        <div className={`cont-main bg_sec sec_1_mob`} style={{
            backgroundColor: bg !== undefined && bg
        }}>
            <div className={`section`} style={{backgroundColor: "inherit"}}>
                <div className="cnt-sec-eight">
                    <div className="top">
                        <div className="heading">
                            <div className="txt">
                                LIFE AT KISTPAY
                            </div>
                        </div>
                        <div className="typo-txt">
                            <div className="p animate reveal">
                                We believe that the best way to develop a person's potential is to provide a positive
                                environment in which
                                they may develop, gain confidence, and push their limits in order to accomplish the
                                seemingly impossible.
                                We encourage our employees to join us on the ground and make a difference in the
                                community.
                            </div>
                        </div>
                    </div>
                    <div className="btm">
                        <div className="sl-hld">
                            {/*<div className="btn-hld">*/}
                            {/*    <img alt={"#"} src={arrow_btn_lft} onClick={(e) => {*/}
                            {/*        e.preventDefault();*/}
                            {/*        // setInit({*/}
                            {/*        //     ...init,*/}
                            {/*        //     isOpen: false,*/}
                            {/*        //     images: [init.images[init.images.length - 1], ...init.images.filter((fl) => fl !== init.images[init.images.length - 1])]*/}
                            {/*        // })*/}
                            {/*    }}/>*/}
                            {/*</div>*/}
                            <div className="slider">
                                {/*{*/}
                                {/*    isOpen && (*/}
                                {/*        SwiperSlider(images)*/}
                                {/*        // <ToggleCard*/}
                                {/*        //     images={images}*/}
                                {/*        //     width="550"*/}
                                {/*        //     height="340"*/}
                                {/*        //     direction={directionToggle}*/}
                                {/*        //     duration={400}*/}
                                {/*        //     className="toggle"*/}
                                {/*        //     isOpen={isOpen}*/}
                                {/*        //     onClick={(node) => {*/}
                                {/*        //         console.log("toggle >>>", node)*/}
                                {/*        //     }}*/}
                                {/*        // />*/}
                                {/*    )*/}
                                {/*}*/}
                                {/*{*/}
                                {/*    SwiperSlider(images, init.isOpen)*/}
                                {/*}*/}
                                <_3DSliderReact/>
                            </div>
                            {/*<div className="btn-hld">*/}
                            {/*    <img alt={"#"} src={arrow_btn_rt} onClick={(e) => {*/}
                            {/*        e.preventDefault();*/}
                            {/*        // setInit({*/}
                            {/*        //     ...init,*/}
                            {/*        //     // isOpen: false,*/}
                            {/*        //     sliderAutoPlay: false,*/}
                            {/*        //     images: [init.images[init.images.length - 1], ...init.images.filter((fl) => fl !== init.images[init.images.length - 1])]*/}
                            {/*        // })*/}
                            {/*    }}/>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default KPHomeSectionEight;