import React from 'react';
import "./KPHSThree.css";
import _SecThree1 from "../../../../images/sections/support/s_1.png";
import _SecThree2 from "../../../../images/sections/support/s_2.png";
import _SecThree3 from "../../../../images/sections/support/s_3.png";
import _SecThree4 from "../../../../images/sections/support/s_4.png";
import _SecThree5 from "../../../../images/sections/support/s_5.png";

const KPHomeSectionThree = ({bg}) => {

    return (
        <div className={`cont-main bg_sec sec_1_mob`} style={{
            backgroundColor: bg !== undefined && bg
        }}>
            <div className={`section`} style={{backgroundColor: "inherit"}}>
                <div className="cnt-sec-three">
                    <div className="rt">
                        <div className="txt-cont">
                            <div className="head">OUR PARTNERS</div>
                            <div className="txt-points">
                                <div className="point">
                                    This integrated journey of the best in business, allows Kistpay to foster an ecosystem of Multifaceted Digitization across Pakistan through Conventional Financial Institutions, Fin Techs, Telecommunications, Insurance, Smartphone Manufacturing, Mobile Retail, and Regulatory bodies.
                                    proprietary mobile device management.
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*<div className="lft">*/}
                    {/*    <div className="icons-hldr">*/}
                    {/*        <img className="ic" alt={"#"} src={_SecThree1}/>*/}
                    {/*        <img className="ic" alt={"#"} src={_SecThree2}/>*/}
                    {/*        <img className="ic" alt={"#"} src={_SecThree3}/>*/}
                    {/*        <img className="ic" alt={"#"} src={_SecThree4}/>*/}
                    {/*        <img className="ic" alt={"#"} src={_SecThree5}/>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </div>
            </div>
        </div>
    );
};

export default KPHomeSectionThree;