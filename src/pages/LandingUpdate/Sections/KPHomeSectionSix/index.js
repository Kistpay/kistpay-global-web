import React from 'react';
import "./KPHomeSectionSix.css";
// import {ToggleCard} from "react-stack-cards";

//prev change
// import image1 from "../../../../images/sections/two/lan_bg_mob_2.jpg";
// import image2 from "../../../../images/sections/two/lan_bg_mob_2__.jpg";
// import image3 from "../../../../images/sections/one/ret_bg_mob_1.jpg";

//next change
// import image1 from "../../../../images/sections/six/1.png";//khuram Bhai
// import image2 from "../../../../images/sections/six/2.png";//faris bhai
// import image3 from "../../../../images/sections/six/3.png";// hassan zafar bhai
// import useMediaQuery from '@mui/material/useMediaQuery';
// import {Navigation, Pagination, Scrollbar, A11y} from 'swiper';
// import {Swiper, SwiperSlide} from "swiper/react";
// import "swiper/css";
// import 'swiper/css/navigation';
// import 'swiper/css/pagination';

import ceoIcn from "../../../../images/sections/six/ceoImg.png";
import qIcn from "../../../../images/sections/six/qIcn.png";


const KPHomeSectionSix = ({bg}) => {
    // const [init, setInit] = useState({
    //     directionToggle: "fanOut",
    //     images: [
    //         {
    //             img: image3,
    //             content: "If you believe in uplifting lives, and want to empower people, then Kistpay is the place for you as we are committed in establishing new infrastructure specially for the undocumented, disconnected population. We here at Kistpay believe that everyone deserves benefits of digitally connected life. Join us and become part of this great challenge in uplifting and converting communities through the power of Digitization."
    //         },
    //         {
    //             img: image1,
    //             content: "Work alongside the best technology solutions providers in the country. We, at Kistpay, will enable you to bring your passion, creativity, and vision to life by being a part of our Digitization journey of Pakistan."
    //         },
    //         {
    //             img: image2,
    //             content: "Work alongside the best technology solutions providers in the country. We, at Kistpay, will enable you to bring your passion, creativity, and vision to life by being a part of our Digitization journey of Pakistan."
    //         }
    //     ],
    //     isOpen: true,
    // });
    // let {directionToggle, images, isOpen} = init;
    // const matches = useMediaQuery('(max-width:767px)');

    // const SwiperSlider = (imagesArr) => {
    //     return (
    //         <div className="swiper" style={{
    //             width: "600px",
    //             height: "300px"
    //         }}>
    //             <Swiper
    //                 modules={[Navigation, Pagination, Scrollbar, A11y]}
    //                 // spaceBetween={50}
    //                 slidesPerView={1}
    //                 navigation
    //                 pagination={{clickable: true}}
    //                 scrollbar={{draggable: true}}
    //                 autoplay={true}
    //                 className="swiper-wrapper"
    //             >
    //                 {/*// <!-- Slides -->*/}
    //                 {
    //                     imagesArr.map(({img, content}) => {
    //                         return (
    //                             <SwiperSlide className="swiper-slide">
    //                                 <div className="oc-hld">
    //                                     <div className="pro-img">
    //                                         <img className="img" alt={"#"} src={img}/>
    //                                     </div>
    //                                     <div className="pra">
    //                                         <div className="p">
    //                                             {content}
    //                                         </div>
    //                                     </div>
    //                                 </div>
    //                             </SwiperSlide>
    //                         )
    //                     })
    //                 }
    //             </Swiper>
    //         </div>
    //     )
    // }

    return (
        <div className={`cont-main bg_sec sec_1_mob`} style={{
            backgroundColor: bg !== undefined && bg
        }}>
            <div className={`section`} style={{backgroundColor: "inherit"}}>
                <div className="cnt-sec-six">
                    <div className="lft">
                        <div className="sl-cont">
                            {/*<ToggleCard*/}
                            {/*    images={images}*/}
                            {/*    width={matches ? "200" : "350"}*/}
                            {/*    height={matches ? "200" : "240"}*/}
                            {/*    direction={directionToggle}*/}
                            {/*    duration={400}*/}
                            {/*    className="toggle"*/}
                            {/*    isOpen={isOpen}*/}
                            {/*/>*/}
                            {/*{SwiperSlider(images)}*/}
                            <img alt={"#"} src={ceoIcn}/>
                        </div>
                    </div>
                    <div className="rt">
                        <div className="tm-sec">
                            <div className="head">
                                <img alt={"#"} src={qIcn}/>
                                {/*JOIN THE KISTPAY CREW*/}
                            </div>
                            <div className="p">
                                At Kistpay we are on a mission to change the world. We're committed to creating social impact for the unbanked by providing them easy access to Smartphone Financing. Kistpay believes everyone deserves digital connectivity.
                                We are growing our dynamic team by the day. If you believe in elevating lives and empowering people, Kistpay is the place for you. We would love for you to be a part of our journey. Email us today at <strong className="st">careers@kistpay.com</strong>
                            </div>
                            {/*<div className="act">*/}
                            {/*    <div className="btn pri">See Open Positions</div>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default KPHomeSectionSix;