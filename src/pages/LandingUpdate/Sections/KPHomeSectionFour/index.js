import React, {useEffect} from 'react';
import "./KPHSFour.css";
import finTechIc from "../../../../images/sections/four/finTechIc1.png";
import bnIc from "../../../../images/sections/four/bnpl1.png";
// import gsIcon from "../../../../images/sections/four/gs1.png";
import dmIc from "../../../../images/sections/four/dm1.png";
// import {
//     animation3DEffect,
//     RevealAnimatedRotateFromLeft,
//     RevealAnimatedRotateFromRight
// } from "../../../../utils/DefaultFunctions";
// import {Parallax} from "react-scroll-parallax";

const KPHomeSectionFour = ({bg}) => {

    useEffect(() => {
        // window.addEventListener("scroll", RevealAnimatedRotateFromLeft);
        // window.addEventListener("scroll", RevealAnimatedRotateFromRight);
        // animation3DEffect();
    }, []);

    return (
        <div className={`cont-main bg_sec sec_1_mob`} style={{
            backgroundColor: bg !== undefined && bg
        }}>
            <div className={`section`} style={{backgroundColor: "inherit"}}>
                <div className="cnt-sec-four">
                    <div className="top">
                        <div className="hdr">
                            <div className="head">REPLACING COMPLEXITY WITH SIMPLICITY</div>
                            {/*<div className="txt-p">*/}
                            {/*    <div className="p">*/}
                            {/*        The Services we Provide*/}
                            {/*    </div>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                    <div className="center">

                        <div className="ic-grp">
                            {/*<Parallax*/}
                            {/*    translateX={["-400px", "0px"]}*/}
                            {/*    scale={[1, 1]}*/}
                            {/*    rotate={[180, 0]}*/}
                            {/*    easing="easeInQuad"*/}
                            {/*>*/}
                            <div className="ic animate reveal-lft">
                                <img alt={"#"} src={finTechIc}/>
                            </div>
                            {/*</Parallax>*/}
                            <div className="p">
                                Fintech
                            </div>
                        </div>
                        <div className="ic-grp">
                            {/*<Parallax*/}
                            {/*    translateX={["-400px", "0px"]}*/}
                            {/*    scale={[1, 1]}*/}
                            {/*    rotate={[180, 0]}*/}
                            {/*    easing="easeInQuad"*/}
                            {/*>*/}
                            <div className="ic animate reveal-lft">
                                <img alt={"#"} src={bnIc}/>
                            </div>
                            {/*</Parallax>*/}
                            <div className="p">
                                BNPL Platform
                            </div>
                        </div>

                        {/*<div className="ic-grp">*/}
                        {/*    /!*<Parallax*!/*/}
                        {/*    /!*    translateX={["400px", "0px"]}*!/*/}
                        {/*    /!*    scale={[1, 1]}*!/*/}
                        {/*    /!*    rotate={[180, 0]}*!/*/}
                        {/*    /!*    easing="easeInQuad"*!/*/}
                        {/*    /!*>*!/*/}
                        {/*    <div className="ic">*/}
                        {/*        <img alt={"#"} src={gsIcon}/>*/}
                        {/*    </div>*/}
                        {/*    /!*</Parallax>*!/*/}
                        {/*    <div className="p">*/}
                        {/*        Device locking technology*/}
                        {/*    </div>*/}
                        {/*</div>*/}


                        <div className="ic-grp">
                            {/*<Parallax*/}
                            {/*    translateX={["400px", "0px"]}*/}
                            {/*    scale={[1, 1]}*/}
                            {/*    rotate={[180, 0]}*/}
                            {/*    easing="easeInQuad"*/}
                            {/*>*/}
                            <div className="ic animate reveal-rt">
                                <img alt={"#"} src={dmIc}/>
                            </div>
                            {/*</Parallax>*/}
                            <div className="p">
                                {/*Device Management*/}
                                {/*through Google DLC*/}
                                Proprietary MDM Technology
                            </div>
                        </div>
                    </div>
                    <div className="btm"/>
                </div>
            </div>
        </div>
    );
};

export default KPHomeSectionFour;