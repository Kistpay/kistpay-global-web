import React from 'react';
import "./KPHomeSectionOne.css";
import _BgImg from "../../../../images/sections/one/bg_1_Kp.png";
// import _BgImg from "../../../../images/sections/one/_bg.png";
// import _MobImg from "../../../../images/sections/one/_mob_2.png";
// import banner_video from "../../../../videos/banner_video.mp4";
import banner_gif from "../../../../images/banner_1.gif";
import downArrowIc from "../../../../images/directionDown.gif";
// import {Animate} from 'react-simple-animate';

const KPHomeSectionOne = ({bg}) => {
    return (
        <div className={`cont-main bg_sec sec_1_mob`} style={{
            backgroundColor: bg !== undefined && bg,
            alignItems: "center",
            flexDirection:"column",
            position: "relative"
        }}>
            <div className="vid-hld">
                {/*<video controls={false} autoPlay="autoplay">*/}
                {/*    <source src={banner_video} type="video/mp4"/>*/}
                {/*</video>*/}
                <img src={banner_gif} alt={"#"}/>
                {/*<section>*/}
                {/*    <video src={banner_video} autoplay controls={false}/>*/}
                {/*</section>*/}
            </div>
            <div className="down-arrow">
                <img alt={"#"} src={downArrowIc}/>
            </div>
            <div className={`section`} style={{backgroundColor: "transparent", position: "absolute"}}>
                <div className="cnt-sec-one">
                    {/*<div className="rt">*/}
                    {/*    <div className="img-hld">*/}
                    {/*        <Animate*/}
                    {/*            start={{transform: 'translate(-300px, 0)'}}*/}
                    {/*            end={{transform: 'translate(20px, 20px)'}}*/}
                    {/*            play>*/}
                    {/*            /!*<img className="_bg" alt={"#"} src={_BgImg}/>*!/*/}
                    {/*            <img className="_bg" alt={"#"} src={_BgImg}/>*/}
                    {/*        </Animate>*/}
                    {/*        /!*<img className="_mob" alt={"#"} src={_MobImg}/>*!/*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    <div className="lft">
                        <div className="cont-box">
                            <div className="top">
                                <div className="p">Enabling Next Billion Users For Digital Connected Life</div>
                                {/*kp-font*/}
                            </div>
                            <div className="btm">
                                {/*<div className="p">*/}
                                {/*    SMARTPHONE ANTI-THEFT SERVICE*/}
                                {/*</div>*/}
                                {/*<div className="actions">*/}
                                {/*    <div className="btn wht">*/}
                                {/*        Read More*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default KPHomeSectionOne;