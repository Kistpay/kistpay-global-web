import React, {useEffect} from 'react';
import KPHeader from "../../SharedComponents/KPHeader";
import KPFooter from "../../SharedComponents/KPFooter";
import KPHomeSectionOne from "./Sections/KPLandingSectionOne";
import KPHomeSectionTwo from "./Sections/KPHomeSectionTwo";
// import KPHomeSectionThree from "./Sections/KPHomeSectionThree";
import KPHomeSectionFour from "./Sections/KPHomeSectionFour";
import KPHomeSectionFive from "./Sections/KPHomeSectionFive";
import KPHomeSectionSix from "./Sections/KPHomeSectionSix";
import KPHomeSectionSeven from "./Sections/KPHomeSectionSeven";
// import KPHomeSectionEight from "./Sections/KPHomeSectionEight";
import "./index.css";
import {
    RevealAnimated,
    RevealAnimatedFadeIn,
    RevealAnimatedRotateFromLeft,
    RevealAnimatedRotateFromRight, showHeaderBackground, toggleScrollIcon
} from "../../utils/DefaultFunctions";
import loadable from "@loadable/component";

const EighthComponent = loadable(() => import('./Sections/KPHomeSectionEight'))

const KPayLanding = () => {
    let isBrowser = typeof window !== 'undefined';

    useEffect(() => {
        if (!isBrowser) return;
        window.addEventListener("scroll", RevealAnimated);
        window.addEventListener("scroll", RevealAnimatedFadeIn);
        window.addEventListener("scroll", RevealAnimatedRotateFromLeft);
        window.addEventListener("scroll", RevealAnimatedRotateFromRight);
        window.addEventListener("scroll", showHeaderBackground);
        // window.addEventListener("scroll", toggleScrollIcon);
    }, [])

    return (
        <div className="main">
            <KPHeader/>
            <KPHomeSectionOne bg={"#447ABB"}/>
            <KPHomeSectionTwo bg={"#4e4e4e"}/>
            {/*<KPHomeSectionThree bg={"#F8F8F8"}/>*/}
            <KPHomeSectionFour bg={"#FFFFFF"}/>
            <KPHomeSectionFive bg={"#FFFFFF"}/>
            <KPHomeSectionSix bg={"#FFFFFF"}/>
            <KPHomeSectionSeven bg={"#447ABB"}/>
            {/*<KPHomeSectionEight bg={"#4e4e4e"}/>*/}
            <EighthComponent bg={"#4e4e4e"}/>
            <KPFooter/>
        </div>
    );
};

export default KPayLanding;