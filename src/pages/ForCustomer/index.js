import React from 'react';
import "../Landing/index.css";
import Header from "../../SharedComponents/Header";
import SectionTwo from "../Landing/Sections/Two";
import SectionFive from "../Landing/Sections/Five";
import Footer from "../../SharedComponents/Footer";
import FCSectionOne from "./Sections/One";
import FCSectionTwo from "./Sections/Two";
import FCSectionThree from "./Sections/Three";
import Quotes from "./Sections/Quotes";
import FCSectionFour from "./Sections/Four";
import FCSectionFive from "./Sections/Five";
import {Helmet} from "react-helmet";

const ForCustomer = () => {
    return (
        <div className="main">
            <Header/>
            {/*this is Section from landing dir*/}
            <SectionTwo bg={"#F1FFF3"}/>

            <FCSectionOne/>

            {/*<FCSectionTwo bg={"#F1FFF3"}/>*/}

            <FCSectionThree/>

            <Quotes/>

            <FCSectionFour bg={"#F1FFF3"}/>

            <FCSectionFive/>

            {/*this is Section from landing dir*/}
            <SectionFive/>
            <Footer/>
        </div>
    );
};

export default ForCustomer;