import React from 'react';
import "./index.css";
import searchIcon from "../../../../images/sections/two/searchIc.png";
import chevRight from "../../../../images/sections/two/chevRight.png";
import ReactGoogleMap from "../../../../SharedComponents/ReactMap";

const FCSectionTwo = ({bg}) => {
    return (
        <div className="section" style={{
            backgroundColor: bg !== undefined && bg,
            width:"100%",
        }}>
            <div className="lft">
                <div className="shp-frm">
                    <div className="hd">
                        <div className="p">Find your nearest Kistpay <div className="p-bold-blue"> Retailer </div></div>
                    </div>
                    <div className="shp">
                        <div className="search-hld">
                            <div className="src-box">
                                <div className="ic">
                                    <img src={searchIcon} alt={"#"}/>
                                </div>
                                <div className="inp">
                                    <input placeholder={"Search By Name"}/>
                                </div>
                            </div>
                        </div>
                        <div className="lst">
                            <div className="counts">
                                Showing <div className="p-bold-green">20 Shops</div>
                            </div>
                            <div className="crd-hld">
                                <div className="crd">
                                    <div className="ttl">
                                        Fahad Khan Communication
                                    </div>
                                    <div className="txt">
                                        <div className="p">
                                            Shop no. 203, Al Nafeesa Mobile Shop, Abdullah Haroon Road, Saddar, Karachi,
                                            Sindh
                                        </div>
                                        <div className="ic">
                                            <img alt={"#"} src={chevRight}/>
                                        </div>
                                    </div>
                                    <div className="sub-txt">
                                        +92 300 1234567
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="rtm">
                <div className="map-hld">

                    <ReactGoogleMap/>

                    <div className="crd-hld">
                        <div className="crd">
                            <div className="ttl">
                                Fahad Khan Communication
                            </div>
                            <div className="txt">
                                <div className="p">
                                    Shop no. 203, Al Nafeesa Mobile Shop, Abdullah Haroon Road, Saddar, Karachi,
                                    Sindh
                                </div>
                            </div>
                            <div className="sub-txt">
                                +92 300 1234567
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FCSectionTwo;