import React from 'react';
import secThreeImg from "../../../../images/sections/three/fcImg_3.png";
import checkIc from "../../../../images/sections/two/checkIc.png";

const FCSectionThree = () => {
    return (
        <div className="section bg_sec cus_sec_1_mob" style={{
            backgroundColor:"white",
            width:"100%",
        }}>
            <div className="rt">
                <div className="img-hld">
                    <img alt={"#"} src={secThreeImg}/>
                </div>
            </div>
            <div className="lft" style={{
                justifyContent:"center"
            }}>
                <div className="cont-box">
                    <div className="top">
                        <div className="p">
                            Why <div className="p-bold-blue"> Kistpay? </div>
                        </div>
                    </div>
                    <div className="btm">
                        <div className="list">
                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-black mob"> Shariah Compliant </div>
                            </div>

                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-black mob"> Interest Free Financing </div>
                            </div>

                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-black mob"> Interest Free Financing </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FCSectionThree;