import React from 'react';

const Quotes = () => {
    return (
        <div className="section" style={{
            backgroundColor:"#33D827",
            color:"white"
        }}>
            <div className="lft">
                <div className="p">
                    "Begin your digital transformation today by acquiring a smartphone through Kistpay. We make smartphone financing easy for our customers through small payments over time.
                </div>
            </div>
        </div>
    );
};

export default Quotes;