import React from 'react';
import "./index.css"
import chevUp from "../../../../images/sections/five/chevUp.png"

const FCSectionFive = () => {
    return (
        <div className="section" style={{
            backgroundColor: "white"
        }}>
            <div className="cnt">
                <div className="hd">
                    Frequent Asked Questions
                </div>
                <div className="list">
                    <div className="tbx on">
                        <div className="p">
                            What do I need to do to get a smartphone?
                        </div>
                        <div className="ic">
                            <img alt={"#"} src={chevUp}/>
                        </div>
                    </div>
                    <div className="tbx">
                        <div className="p">
                            Go to your nearest Kistpay Retailer to initiate your smartphone financing application.
                        </div>
                        {/*<div className="ic">*/}
                        {/*    <img alt={"#"} src={chevUp}/>*/}
                        {/*</div>*/}
                    </div>

                    <div className="tbx on">
                        <div className="p">
                            What documents do I need to apply for smartphone financing?
                        </div>
                        <div className="ic">
                            <img alt={"#"} src={chevUp}/>
                        </div>
                    </div>

                    <div className="tbx">
                        <div className="p">
                            You are required to bring your ID card, Proof of Income, a copy of your utility bill, CNIC,
                            and contact number of 2 references.
                        </div>
                        {/*<div className="ic">*/}
                        {/*    <img alt={"#"} src={chevUp}/>*/}
                        {/*</div>*/}
                    </div>

                    <div className="tbx on">
                        <div className="p">
                            How much time will it take to process the application?
                        </div>
                        <div className="ic">
                            <img alt={"#"} src={chevUp}/>
                        </div>
                    </div>

                    <div className="tbx">
                        <div className="p">
                            Once your application is submitted, our loan officer will verify your information and
                            approve/ disapprove your application within 24 hours.
                        </div>
                        {/*<div className="ic">*/}
                        {/*    <img alt={"#"} src={chevUp}/>*/}
                        {/*</div>*/}
                    </div>

                    <div className="tbx on">
                        <div className="p">
                            How many installments plans are available for smartphone financing?
                        </div>
                        <div className="ic">
                            <img alt={"#"} src={chevUp}/>
                        </div>
                    </div>

                    <div className="tbx">
                        <div className="p">
                            Our standard installment plan is of 3 months with 40% Down Payment.
                            {/*<div className="ic">*/}
                            {/*    <img alt={"#"} src={chevUp}/>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                </div>
                </div>
            </div>
            );
            };

            export default FCSectionFive