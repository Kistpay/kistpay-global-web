import React from 'react';
import DefaultSlider from "../../../../SharedComponents/Slider";

const FCSectionFour = ({bg}) => {
    return (
        <div className="img-section" >
            <div className="slider-hld" style={{
                backgroundColor: bg !== undefined && bg
            }}>
                <div className="top">
                    <div className="p">
                        Millions of <div className="p-bold-blue"> Happy Customers </div> and  <div className="p-bold-green"> Counting </div>
                    </div>
                </div>
                <div className="btm">
                    <DefaultSlider data={[{
                        title:"Muhammad Tabarak",
                        content:"I own a small dry-cleaning shop at Badar Commercial, I am 45 years old and have been working for the last 30 years. I have never owned a Smartphone in my life, a main reason was that I could not afford it on my monthly income and I can not go to a bank asking for a loan. This is my first time using a Smartphone that I have been able to purchase because of Kistpay. They did not ask for my income for a whole year, neither did they need any documentation from my employers, I just went to my neighbor's shop, who is a Kistpay Retailer and bought a Smartphone on installments by giving him a copy of my CNIC, copy of utility bill, 2 reference CNICs and proof of my business."
                    }]}/>
                </div>
            </div>
        </div>
    );
};

export default FCSectionFour;