import React from 'react';
import "./index.css";
import fc_1 from "../../../../images/sections/one/fc_1.png";
import fc_2 from "../../../../images/sections/one/fc_2.png";
import fc_3 from "../../../../images/sections/one/fc_3.png";

const FCSectionOne = ({bg}) => {
    return (
        <div className="section bg_sec_df df_bg" style={{
            backgroundColor: bg !== undefined ? bg : "white",
            justifyContent:"center",
            width:"100%",
        }}>
            <div className="clm">
                <div className="rw">
                    <div className="p">
                        Get your Smartphone <div className="p-bold-green"> Today </div>
                    </div>
                </div>
                <div className="rw">
                    <div className="box">
                        <div className="t">
                            <div className="ic">
                                <img src={fc_1} alt={"#"}/>
                            </div>
                        </div>
                        <div className="c">
                            <div className="hd">
                                Find a Kistpay Retailer
                            </div>
                            <div className="p">
                                Find your nearest Kistpay Retailer to get your Smartphone now
                            </div>
                        </div>
                        <div className="b">
                            <div className="btn sec">
                                FIND NEARBY RETAILER
                            </div>
                        </div>
                    </div>

                    <div className="box">
                        <div className="t">
                            <div className="ic">
                                <img src={fc_2} alt={"#"}/>
                            </div>
                        </div>
                        <div className="c">
                            <div className="hd">
                                Apply for Financing
                            </div>
                            <div className="p">
                                Choose a Smartphone that you like and fill our Application form to apply for Financing in few easy steps
                            </div>
                        </div>
                        <div className="b">
                            <div className="btn sec">
                                VIEW SMARTPHONE
                            </div>
                        </div>
                    </div>

                    <div className="box">
                        <div className="t">
                            <div className="ic">
                                <img src={fc_3} alt={"#"}/>
                            </div>
                        </div>
                        <div className="c">
                            <div className="hd">
                                Get Approval in a day
                            </div>
                            <div className="p">
                                Our Loan Officer will verify your information, and you will receive your Smartphone as soon as the application is approved.                            </div>
                        </div>
                        <div className="b">
                            <div className="btn sec">
                                VIEW TERMS & CONDITIONS
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FCSectionOne;