import React from 'react';
import checkIc from "../../../../images/sections/two/checkIc.png";
import CareerSecOneImg from "../../../../images/sections/one/careerSecOneImg.png";

const CareerOne = () => {
    return (
        <div className="section" style={{
            backgroundColor: "#F1F6FF"
        }}>
            <div className="lft">
                <div className="cont-box">
                    <div className="top">
                        <div className="p">
                            Become a part of Kistpay Enabling Lives
                            <div className="p-bold-blue"> Ecosystem!</div>
                        </div>
                    </div>
                    <div className="btm">
                        <div className="p" style={{whiteSpace:"pre-wrap", fontSize:"20px"}}>
                            If you wish to become a part of a journey that is spread across continents, is enriching and uplifting the lives of individuals who do not have access to conventional financing. We want to hear how you can make a difference in this ecosystem with us.
                        </div>
                        <div className="actions">
                            <div className="btn pri">
                                SEE OPENINGS
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="rt">
                <div className="img-hld">
                    <img alt={"#"} src={CareerSecOneImg}/>
                </div>
            </div>
        </div>
    );
};

export default CareerOne;