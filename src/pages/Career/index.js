import React from 'react';
import "../Landing/index.css";
import Header from "../../SharedComponents/Header";
import SectionFive from "../Landing/Sections/Five";
import Footer from "../../SharedComponents/Footer";
import CareerOne from "./Sections/One";
import CareerTwo from "./Sections/Two";

const Career = () => {
    return (
        <div className="main">
            <Header/>
            <CareerOne/>

            <CareerTwo/>

            {/*this is Section from landing dir*/}
            <SectionFive/>
            <Footer/>
        </div>
    );
};

export default Career;