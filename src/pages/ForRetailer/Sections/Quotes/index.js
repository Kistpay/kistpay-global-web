import React from 'react';
import "./index.css";

const FRSectionQuotes = () => {
    return (
        <div className="section" style={{
            backgroundColor:"#4979DF",
            color:"white"
        }}>
            <div className="lft" style={{
                flexDirection:"column"
            }}>
                <div className="p">
                    "Kistpay aims to enable its Retailers and support them to navigate & optimize their digital transformation journey.
                </div>
                <div className="sgn">
                    <div className="txt">- Syed Asif Jafri</div>
                    <div className="sub-txt">CEO & Founder</div>
                </div>
            </div>
        </div>
    );
};

export default FRSectionQuotes;