import React, {useEffect, useState} from 'react';
import {Slide} from "react-slideshow-image";

const defaultState = {
    slides: []
}

const ContentSlider = (props) => {
    let {arrows = false, indicators = true, contents} = props;
    const [init, setInit] = useState(defaultState);

    useEffect(() => {
        if (contents) {
            setInit({
                ...init,
                slides: contents
            })
        }
    }, [])

    return (
        <div className="slides">
            <Slide easing="ease" arrows={arrows} indicators={indicators}>
                {
                    init.slides.map((content) => {
                        return (
                            content
                        )
                    })
                }
            </Slide>
        </div>
    );
};

export default ContentSlider;