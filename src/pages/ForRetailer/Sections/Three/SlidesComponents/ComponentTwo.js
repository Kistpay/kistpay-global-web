import React from 'react';
import slide_img_1 from "../../../../../images/sections/three/slide_img_1.png";


const ComponentTwo = () => {
    return (
        <div className="section" style={{
            backgroundColor:"#F1F6FF",
        }}>
            <div className="lft">
                <div className="cont-box">
                    <div className="top">
                        <div className="p">
                            <div className="p-bold-blue"> Manage your Daily Stock </div>
                        </div>
                    </div>
                    <div className="btm">
                        <div className="list">
                            <div className="p-black"> You can now manage your daily inventory including Report Generation. </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="rt">
                <div className="img-hld" >
                {/*    style={{*/}
                {/*    width:"464px",height:"496px"*/}
                {/*}}*/}
                    <img alt={"#"} src={slide_img_1}/>
                </div>
            </div>
        </div>
    );
};

export default ComponentTwo;