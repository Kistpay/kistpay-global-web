import React from 'react';
import "./index.css";
import ContentSlider from "./Slider";
import ComponentOne from "./SlidesComponents/ComponentOne";
import ComponentTwo from "./SlidesComponents/ComponentTwo";

const FRSectionThree = () => {
    return (
        <div className="img-section">
            <div className="slider-hld" style={{backgroundColor:"#F1F6FF",padding:"3rem 6rem"}}>
                <div className="btm">
                    <ContentSlider
                        arrows={true}
                        contents={[
                            <ComponentOne/>,
                            <ComponentTwo/>
                        ]}
                    />
                </div>
            </div>
        </div>
    );
};

export default FRSectionThree;