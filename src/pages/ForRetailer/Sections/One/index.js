import React from 'react';
import secThreeImg from "../../../../images/sections/one/secImgfr_1.png";
import checkIc from "../../../../images/sections/two/checkIc.png";

const FRSectionOne = () => {
    return (
        <div className="section bg_sec ret_sec_1_mob" style={{
            backgroundColor:"#F1F6FF"
        }}>
            <div className="lft">
                <div className="cont-box">
                    <div className="top">
                        <div className="p">
                            <div className="p-bold-blue"> Enabling Retailers </div> to expand their customer base with <div className="p-bold-green"> Shariah Compliant </div> and <div className="p-bold-green"> Interest-Free Financing </div>
                        </div>
                    </div>
                    <div className="btm">
                        <div className="list">
                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-blue mob"> Sell smartphones without holding massive inventory </div>
                            </div>

                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-blue mob"> Manage day to day Expense and Credit </div>
                            </div>

                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-blue mob"> Enrich lives in your community </div>
                            </div>

                        </div>
                        <div className="actions">
                            <div className="btn pri" onClick={()=> window.open(`https://play.google.com/store/apps/details?id=com.kistpay`,"_blank")}>
                                Get App For Free
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="rt">
                <div className="img-hld">
                    <img alt={"#"} src={secThreeImg}/>
                </div>
            </div>
        </div>
    );
};

export default FRSectionOne;