import React, {useEffect, useRef, useState} from 'react';
import "./index.css";
import f1g from "../../../../images/forms_icons/f_1_g.png";
import f1b from "../../../../images/forms_icons/f_1_b.png";
import f2g from "../../../../images/forms_icons/f_2_g.png";
import f2b from "../../../../images/forms_icons/f_2_b.png";
import f3g from "../../../../images/forms_icons/f_3_g.png";
import f3b from "../../../../images/forms_icons/f_3_b.png";
import f4g from "../../../../images/forms_icons/f_4_g.png";
import f4b from "../../../../images/forms_icons/f_4_b.png";
import f5g from "../../../../images/forms_icons/f_5_g.png";
import f5b from "../../../../images/forms_icons/f_5_b.png";
import f6g from "../../../../images/forms_icons/f_6_g.png";
import f6b from "../../../../images/forms_icons/f_6_b.png";
import f7g from "../../../../images/forms_icons/f_7_g.png";
import f7b from "../../../../images/forms_icons/f_7_b.png";
import {API} from "../../../../utils/service";


const defaultState = {
    f1: false, f2: false, f3: false, f4: false, f5: false, f6: false, f7: false, f8: false,
    "address": "",
    "city": "",
    "cnicNumber": "",
    "email": "",
    "fullName": "",
    "phoneNumber": "",
    "shopName": "",
    "shopType": ""
}
const FRSectionFive = () => {
    const [init, setInit] = useState(defaultState);
    let {f1, f2, f3, f4, f5, f6, f7, f8} = init;

    const handleActiveInput = (inp, type) => {
        switch (inp) {
            case "f1":
                if (type === 1) {
                    setInit({
                        ...init,
                        f1: true
                    });
                } else {
                    setInit({
                        ...init,
                        f1: false
                    });
                }
                break;
            case "f2":
                if (type === 1) {
                    setInit({
                        ...init,
                        f2: true
                    });
                } else {
                    setInit({
                        ...init,
                        f2: false
                    });
                }
                break;
            case "f3":
                if (type === 1) {
                    setInit({
                        ...init,
                        f3: true
                    });
                } else {
                    setInit({
                        ...init,
                        f3: false
                    });
                }
                break;
            case "f4":
                if (type === 1) {
                    setInit({
                        ...init,
                        f4: true
                    });
                } else {
                    setInit({
                        ...init,
                        f4: false
                    });
                }
                break;
            case "f5":
                if (type === 1) {
                    setInit({
                        ...init,
                        f5: true
                    });
                } else {
                    setInit({
                        ...init,
                        f5: false
                    });
                }
                break;
            case "f6":
                if (type === 1) {
                    setInit({
                        ...init,
                        f6: true
                    });
                } else {
                    setInit({
                        ...init,
                        f6: false
                    });
                }
                break;
            case "f7":
                if (type === 1) {
                    setInit({
                        ...init,
                        f7: true
                    });
                } else {
                    setInit({
                        ...init,
                        f7: false
                    });
                }
                break;
            default:
                return ""
        }
    }

    const handleChange = (e) => {
        setInit({
            ...init,
            [e.target.name]: e.target.value
        });
    }

    const validation = (obj) => {
        let valid = {error: false, message: ""};
        let email_regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

        if (obj.email === "") {
            valid.error = true;
            valid.message += valid.message ? "\nEmail Are Required" : "Email Are Required"
        } else if (obj.email !== "") {
            if (!email_regex.test(obj.email)) {
                valid.error = true;
                valid.message += valid.message ? "\nWrong Email Format Example: example@mail.com" : "Wrong Email Format Example: example@mail.com"
            }
        }
        if (obj.fullName === "") {
            valid.error = true;
            valid.message += valid.message ? "\nName is Required" : "Name is Required";
        }
        if (obj.phoneNumber === "") {
            valid.error = true;
            valid.message += valid.message ? "\nPhone Number is Required" : "Phone Number is Required";
        }
        if (obj.shopName === "") {
            valid.error = true;
            valid.message += valid.message ? "\nShop Name is Required" : "Shop Name is Required";
        }
        if (obj.shopType === "") {
            valid.error = true;
            valid.message += valid.message ? "\nShop Type is Required" : "Shop Type is Required";
        }
        if (obj.address === "") {
            valid.error = true;
            valid.message += valid.message ? "\nAddress is Required" : "Address is Required";
        }

        if (obj.cnicNumber === "") {
            valid.error = true;
            valid.message += valid.message ? "\nCNIC Number is Required" : "CNIC Number is Required";
        }
        if (obj.city === "") {
            valid.error = true;
            valid.message += valid.message ? "\nCity is Required" : "City is Required";
        }

        return valid;
    }

    const handleSubmit = (obj) => {
        let validate = validation(obj);
        if (!validate.error) {
            API.post(`/kistpay-website-controller/submit-retailer-lead`, obj).then((res) => {
                console.log("handleSubmit", res)
                if (res.status === 200) {
                    console.log("handleSubmit", obj)
                    setInit({
                        ...init,
                        "address": "",
                        "city": "",
                        "cnicNumber": "",
                        "email": "",
                        "fullName": "",
                        "phoneNumber": "",
                        "shopName": "",
                        "shopType": ""
                    })
                    alert("Request Successfully Submitted");
                } else {
                    alert(res.data.message)
                }
            })
        } else {
            alert(validate.message)
        }
    }

    return (
        <div className="section" style={{
            backgroundColor: "white",
            flexDirection: "column",
            padding: "3rem 6rem"
        }}>
            <div className="top">
                <div className="p">
                    Become a Kistpay Retailer <div className="p-bold-blue"> Today </div>
                </div>
                <div className="txt">
                    Fill out the form below to sign up with Kistpay and get access to our Retailer App.
                </div>
            </div>
            <div className="btm">
                <div className="frm">
                    <div className={`inp-grp ${f1 && "on"}`}>
                        <div className="ic">
                            {!f1 ? <img alt={""} src={f1g}/> : <img alt={""} src={f1b}/>}
                        </div>
                        <div className="inp">
                            <input type={"text"} value={init.fullName} name={"fullName"} onChange={handleChange}
                                   onFocus={() => handleActiveInput("f1", 1)}
                                   onBlur={() => handleActiveInput("f1", 2)} placeholder={"Enter your Full Name"}/>
                        </div>
                    </div>

                    <div className={`inp-grp ${f2 && "on"}`}>
                        <div className="ic">
                            {!f2 ? <img alt={""} src={f2g}/> : <img alt={""} src={f2b}/>}
                        </div>
                        <div className="inp">
                            <input type={"text"} value={init.shopName} name={"shopName"} onChange={handleChange}
                                   onFocus={() => handleActiveInput("f2", 1)}
                                   onBlur={() => handleActiveInput("f2", 2)} placeholder={"Enter your Shop Name"}/>
                        </div>
                    </div>

                    <div className={`inp-grp ${f3 && "on"}`}>
                        <div className="ic">
                            {!f3 ? <img alt={""} src={f3g}/> : <img alt={""} src={f3b}/>}
                        </div>
                        <div className="inp">
                            <input type={"text"} value={init.phoneNumber} name={"phoneNumber"} onChange={handleChange}
                                   onFocus={() => handleActiveInput("f3", 1)}
                                   onBlur={() => handleActiveInput("f3", 2)} placeholder={"Enter your Phone Number"}/>
                        </div>
                    </div>

                    <div className={`inp-grp ${f4 && "on"}`}>
                        <div className="ic">
                            {!f4 ? <img alt={""} src={f2g}/> : <img alt={""} src={f2b}/>}
                        </div>
                        <div className="inp">
                            <input type={"text"} value={init.shopType} name={"shopType"} onChange={handleChange}
                                   onFocus={() => handleActiveInput("f4", 1)}
                                   onBlur={() => handleActiveInput("f4", 2)} placeholder={"Select Shop Type"}/>
                        </div>
                    </div>

                    <div className={`inp-grp ${f5 && "on"}`}>
                        <div className="ic">
                            {!f5 ? <img alt={""} src={f4g}/> : <img alt={""} src={f4b}/>}
                        </div>
                        <div className="inp">
                            <input type={"text"} value={init.cnicNumber} name={"cnicNumber"} onChange={handleChange}
                                   onFocus={() => handleActiveInput("f5", 1)}
                                   onBlur={() => handleActiveInput("f5", 2)} placeholder={"Enter your CNIC Number"}/>
                        </div>
                    </div>

                    <div className={`inp-grp ${f6 && "on"}`}>
                        <div className="ic">
                            {!f6 ? <img alt={""} src={f5g}/> : <img alt={""} src={f5b}/>}
                        </div>
                        <div className="inp">
                            <input type={"text"} value={init.city} name={"city"} onChange={handleChange}
                                   onFocus={() => handleActiveInput("f6", 1)}
                                   onBlur={() => handleActiveInput("f6", 2)} placeholder={"Enter your City Name"}/>
                        </div>
                    </div>

                    <div className={`inp-grp ${f7 && "on"}`}>
                        <div className="ic">
                            {!f7 ? <img alt={""} src={f6g}/> : <img alt={""} src={f6b}/>}
                        </div>
                        <div className="inp">
                            <input type={"text"} value={init.email} name={"email"} onChange={handleChange}
                                   onFocus={() => handleActiveInput("f7", 1)}
                                   onBlur={() => handleActiveInput("f7", 2)} placeholder={"Enter your Email Address"}/>
                        </div>
                    </div>

                    <div className={`inp-grp ${f8 && "on"}`}>
                        <div className="ic">
                            {!f8 ? <img alt={""} src={f7g}/> : <img alt={""} src={f7b}/>}
                        </div>
                        <div className="inp">
                            <input type={"text"} value={init.address} name={"address"} onChange={handleChange}
                                   onFocus={() => handleActiveInput("f8", 1)}
                                   onBlur={() => handleActiveInput("f8", 2)} placeholder={"Enter Shop Address"}/>
                        </div>
                    </div>
                </div>
                <div className="actions">
                    <div className="btn pri" style={{
                        margin: "0"
                    }} onClick={() => {
                        handleSubmit({
                            "address": init.address,
                            "city": init.city,
                            "cnicNumber": init.cnicNumber,
                            "email": init.email,
                            "fullName": init.fullName,
                            "phoneNumber": init.phoneNumber,
                            "shopName": init.shopName,
                            "shopType": init.shopType
                        })
                    }}>
                        SUBMIT
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FRSectionFive;