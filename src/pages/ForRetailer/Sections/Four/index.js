import React from 'react';
import DefaultSlider from "../../../../SharedComponents/Slider";

const FRSectionFour = () => {
    return (
        <div className="img-section">
            <div className="slider-hld" style={{
                backgroundColor: "white"
            }}>
                <div className="top">
                    <div className="p">
                        Thousands of <div className="p-bold-blue"> Satisfied Retailers </div> and <div
                        className="p-bold-green"> Counting </div>
                    </div>
                </div>
                <div className="btm">
                    <DefaultSlider data={[{
                        title: "Danish Hashmi",
                        content: "My name is Danish Hashmi and I am a proud Kistpay Retailer. I have been in the Mobile Retail business for about 12 years, but never have I ever sold one single mobile phone on credit due to interest. With Kistpay, I now sell Smartphones on Installments guilt-free, as the platform is completely Shariah Compliant and Interest-Free."
                    },{
                        title: "Salman Amir",
                        content: "My name is Salman Amir and I am a proud Kistpay Retailer. I have been in the Mobile Retail business for about 12 years, but never have I ever sold one single mobile phone on credit due to interest. With Kistpay, I now sell Smartphones on Installments guilt-free, as the platform is completely Shariah Compliant and Interest-Free."
                    }]}/>
                </div>
            </div>
        </div>
    );
};

export default FRSectionFour;