import React from 'react';
import "../../../ForCustomer/Sections/One/index.css";
import fr_1 from "../../../../images/sections/two/fr_1.png";
import fr_2 from "../../../../images/sections/two/fr_2.png";
import fr_3 from "../../../../images/sections/two/fr_3.png";

const FRSectionTwo = ({bg}) => {
    return (
        <div className="section bg_sec_df df_bg" style={{
            backgroundColor: bg !== undefined ? "white" : "white",
            justifyContent:"center",
            width:"100%"
        }}>
            <div className="clm">
                <div className="rw">
                    <div className="p">
                        How to become a <div className="p-bold-blue"> Kistpay Retailer? </div>
                    </div>
                </div>
                <div className="rw">
                    <div className="box" style={{
                        backgroundColor: bg !== undefined ? bg : "white",
                    }}>
                        <div className="t">
                            <div className="ic">
                                <img src={fr_1} alt={"#"}/>
                            </div>
                        </div>
                        <div className="c">
                            <div className="hd">
                                Get Kistpay App
                            </div>
                            <div className="p">
                                Download Kistpay Mobile App from Apple Store or Google Play Store to sign-up
                            </div>
                        </div>
                        <div className="b">
                            <div className="btn pri" onClick={()=> window.open(`https://play.google.com/store/apps/details?id=com.kistpay`,"_blank")}>
                                DOWNLOAD APP
                            </div>
                        </div>
                    </div>

                    <div className="box" style={{
                        backgroundColor: bg !== undefined ? bg : "white",
                    }}>
                        <div className="t">
                            <div className="ic">
                                <img src={fr_2} alt={"#"}/>
                            </div>
                        </div>
                        <div className="c">
                            <div className="hd">
                                Sign up as Kistpay Retailer
                            </div>
                            <div className="p">
                                Fill in the required information for yourself & your store to sign up
                            </div>
                        </div>
                        <div className="b">
                            <div className="btn pri">
                                REGISTER HERE
                            </div>
                        </div>
                    </div>

                    <div className="box" style={{
                        backgroundColor: bg !== undefined ? bg : "white",
                    }}>
                        <div className="t">
                            <div className="ic">
                                <img src={fr_3} alt={"#"}/>
                            </div>
                        </div>
                        <div className="c">
                            <div className="hd">
                                Welcome to Kistpay Retailer Network
                            </div>
                            <div className="p">You now have access to Kistpay Retailer Application. Manage all your daily tasks, sales, loans, and credits on just one application </div>
                        </div>
                        <div className="b">
                            <div className="btn pri">
                                FIND FEATURES
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default FRSectionTwo;