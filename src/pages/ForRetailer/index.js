import React from 'react';
import "../Landing/index.css";
import Header from "../../SharedComponents/Header";
import SectionFive from "../Landing/Sections/Five";
import Footer from "../../SharedComponents/Footer";
import FRSectionOne from "./Sections/One";
import FRSectionTwo from "./Sections/Two";
import FRSectionThree from "./Sections/Three";
import FRSectionQuotes from "./Sections/Quotes";
import FRSectionFour from "./Sections/Four";
import FRSectionFive from "./Sections/Five";

const ForRetailer = () => {
    return (
        <div className="main">
            <Header/>

            <FRSectionOne/>

            <FRSectionTwo bg={"#F1F6FF"}/>

            <FRSectionThree/>

            <FRSectionQuotes/>

            <FRSectionFour/>

            <FRSectionFive/>

            {/*this is Section from landing dir*/}
            <SectionFive/>
            <Footer/>
        </div>
    );
};

export default ForRetailer;