import * as React from "react";
import "../Styles/index.css";
import 'react-slideshow-image/dist/styles.css';
// import Landing from "./Landing";
import {ParallaxProvider} from "react-scroll-parallax";
import KPayLanding from "./LandingUpdate";

// styles
const pageStyles = {
    display: "flex",
    justifyContent: "center",
    color: "#232129",
    fontFamily: "poppins",
    overflow:"hidden"
}

// markup
const IndexPage = () => {
    return (
        <ParallaxProvider>
            <main style={pageStyles}>
                {/*<Landing/>*/}
                <KPayLanding/>
            </main>
        </ParallaxProvider>
    )
}

export default IndexPage
