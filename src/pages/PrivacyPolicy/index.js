import React from 'react';
import "../Landing/index.css";
import "../TermsAndCondition/index.css";
import Header from "../../SharedComponents/Header";
import SectionFive from "../Landing/Sections/Five";
import Footer from "../../SharedComponents/Footer";
import SectionOnePrivacyPolicy from "./Sections/one";

const PrivacyPolicy = () => {
    return (
        <div className="main">
            <Header/>

            <SectionOnePrivacyPolicy/>

            {/*this is Section from landing dir*/}
            <SectionFive/>
            <Footer/>
        </div>
    );
};

export default PrivacyPolicy;