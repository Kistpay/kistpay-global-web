import React from 'react';
import company_img_1 from "../../../../images/sections/one/company_img_1.png";
import "./index.css";

const CSectionOne = () => {
    return (
        <div className="section" style={{
            flexDirection:"column",
            padding:"3rem 23rem"
        }}>
            <div className="top">
                <div className="p">
                    Kistpay works in the emerging <div className="p-bold"> MENAP </div> region to enable more than <div className="p-bold-blue"> 600 million lives </div>
                </div>
            </div>
            <div className="btm">
                <img alt={"#"} src={company_img_1}/>
            </div>
        </div>
    );
};

export default CSectionOne;