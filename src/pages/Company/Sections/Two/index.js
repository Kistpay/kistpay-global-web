import React from 'react';
import "./index.css";
import poster_vid from "../../../../images/sections/two/poster_vid.png";

const CSectionTwo = () => {
    return (
        <div className="section" style={{
            backgroundColor:"white"
        }}>
            <div className="ov-hld bg_sec df_bg">
                <div className="p">
                    Our <div className="p-bold-blue"> Vision </div>
                </div>
                <div className="p-bx">
                    <div className="hd"> Enabling Lives </div>
                    <div className="cnt"> Shariah Compliant, Interest-free Financing to enrich and enable the lives of masses with low or undocumented income across the world. </div>
                </div>
                <div className="vd-cnt">
                    <section>
                        <video src={"#"} poster={poster_vid} autoPlay controls/>
                    </section>
                </div>
            </div>
        </div>
    );
};

export default CSectionTwo;