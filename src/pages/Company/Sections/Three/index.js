import React from 'react';
import "./index.css";
import team_3 from "../../../../images/sections/three/team_3.png";

const CSectionThree = () => {
    return (
        <div className="section" style={{
            backgroundImage: "linear-gradient(to left, #002B84, #2760D8)",
            width:"100%"
        }}>
            <div className="tm-hld">
                <div className="p">
                    Founding <div className="p-white" style={{margin:"0 0.5rem"}}> Team </div>
                </div>

                <div className="tm-img-bx">
                    <img alt={"#"} src={team_3}/>
                </div>

                <div className="tm-btm">
                    <div className="p">
                        Want to Join our <div className="p-white" style={{margin:"0 0.5rem"}}> Team? </div>
                    </div>
                    <div className="btn wht">
                        SEE THE JOB OPENINGS
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CSectionThree;