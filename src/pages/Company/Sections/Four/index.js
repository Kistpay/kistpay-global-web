import React from 'react';
import "../Two/index.css";
import lc_blue from "../../../../images/lc_blue.png";
import ph_grey from "../../../../images/ph_grey.png";
import em_grey from "../../../../images/em_grey.png";

const CSectionFour = () => {
    return (
        <div className="section" style={{
            backgroundColor: "white"
        }}>
            <div className="ov-hld bg_sec df_bg">
                <div className="p">
                    Our <div className="p-bold-blue"> Office Location </div>
                </div>
                <div className="p-bx">
                    <div className="hd"> Head Office</div>
                    <div className="lc">
                        <div className="ic">
                            <img alt={"#"} src={lc_blue}/>
                        </div>
                        <div className="p blue">
                            Karachi, Pakistan
                        </div>
                    </div>
                    <div className="cnt" style={{
                        fontSize:"16px"
                    }}> 29 C, Mezzanine floor, Near master juice,, Sunset Lane 1, Karachi, Pakistan،
                        Phase 2 Commercial Area Defence Housing Authority, Karachi, Karachi City, Sindh 75500
                    </div>
                    <div className="lc">
                        <div className="ic">
                            <img alt={"#"} src={ph_grey}/>
                        </div>
                        <div className="sp">
                            +92 (21) 31234567
                        </div>
                    </div>

                    <div className="lc">
                        <div className="ic">
                            <img alt={"#"} src={em_grey}/>
                        </div>
                        <div className="sp">
                            info@kistpay.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CSectionFour;