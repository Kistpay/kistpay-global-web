import React from 'react';
import Header from "../../SharedComponents/Header";
import SectionFive from "../Landing/Sections/Five";
import Footer from "../../SharedComponents/Footer";
import CSectionOne from "./Sections/One";
import CSectionTwo from "./Sections/Two";
import CSectionThree from "./Sections/Three";
import CSectionFour from "./Sections/Four";

const Company = () => {
    return (
        <div className="main">
            <Header/>

            <CSectionOne/>

            <CSectionTwo/>

            <CSectionThree/>

            <CSectionFour/>

            {/*this is Section from landing dir*/}
            <SectionFive/>
            <Footer/>
        </div>
    );
};

export default Company;