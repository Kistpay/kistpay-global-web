import React, {useState} from 'react';
import "./index.css";
import {Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, IconButton} from "@mui/material";
import MuiButton from "../../SharedComponents/MuiButton/Button";
import CancelPresentationIcon from '@mui/icons-material/CancelPresentation';
import f1g from "../../images/forms_icons/f_1_g.png";
import f1b from "../../images/forms_icons/f_1_b.png";
import f3g from "../../images/forms_icons/f_3_g.png";
import f3b from "../../images/forms_icons/f_3_b.png";
import f6g from "../../images/forms_icons/f_6_g.png";
import f6b from "../../images/forms_icons/f_6_b.png";
import f8g from "../../images/forms_icons/f_8_g.png";
import f8b from "../../images/forms_icons/f_8_b.png";
import {API} from "../../utils/service";

const defaultState = {
    f1: false, f2: false, f3: false, f4: false, f5: false, f6: false, f7: false, f8: false,
    "email": "",
    "fullName": "",
    "phoneNumber": "",
    "query": ""
}

const ContactUsDialog = (props) => {
    let {
        open,
        title,
        description,
        content,
        handleClose,
        actionLabel = "Submit",
        maxWidth = "sm",
        width,
    } = props;

    const [init, setInit] = useState(defaultState);
    let {f1, f2, f3, f4, f5, f6, f7, f8} = init;

    const handleActiveInput = (inp, type) => {
        switch (inp) {
            case "f1":
                if (type === 1) {
                    setInit({
                        ...init,
                        f1: true
                    });
                } else {
                    setInit({
                        ...init,
                        f1: false
                    });
                }
                break;
            case "f2":
                if (type === 1) {
                    setInit({
                        ...init,
                        f2: true
                    });
                } else {
                    setInit({
                        ...init,
                        f2: false
                    });
                }
                break;
            case "f3":
                if (type === 1) {
                    setInit({
                        ...init,
                        f3: true
                    });
                } else {
                    setInit({
                        ...init,
                        f3: false
                    });
                }
                break;
            case "f4":
                if (type === 1) {
                    setInit({
                        ...init,
                        f4: true
                    });
                } else {
                    setInit({
                        ...init,
                        f4: false
                    });
                }
                break;
            case "f5":
                if (type === 1) {
                    setInit({
                        ...init,
                        f5: true
                    });
                } else {
                    setInit({
                        ...init,
                        f5: false
                    });
                }
                break;
            case "f6":
                if (type === 1) {
                    setInit({
                        ...init,
                        f6: true
                    });
                } else {
                    setInit({
                        ...init,
                        f6: false
                    });
                }
                break;
            case "f7":
                if (type === 1) {
                    setInit({
                        ...init,
                        f7: true
                    });
                } else {
                    setInit({
                        ...init,
                        f7: false
                    });
                }
                break;
            default:
                return ""
        }
    }

    const handleChange = (e) => {
        setInit({
            ...init,
            [e.target.name]: e.target.value
        })
    }

    const validation = (obj) => {
        let valid = {error: false, message: ""};
        let email_regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

        if (obj.email === "") {
            valid.error = true;
            valid.message += valid.message ? "\nEmail Are Required" : "Email Are Required"
        }else if (obj.email !== "") {
            if (!email_regex.test(obj.email)) {
                valid.error = true;
                valid.message += valid.message ? "\nWrong Email Format Example: example@mail.com" : "Wrong Email Format Example: example@mail.com"
            }
        }
        if (obj.fullName === "") {
            valid.error = true;
            valid.message += valid.message ? "\nName is Required" : "Name is Required";
        }
        if (obj.phoneNumber === "") {
            valid.error = true;
            valid.message += valid.message ? "\nPhone Number is Required" : "Phone Number is Required";
        }
        if (obj.query === "") {
            valid.error = true;
            valid.message += valid.message ? "\nPlease Write an Query" : "Please Write an Query";
        }
        return valid;
    }

    const handleSubmit = (obj) => {

        let validate = validation(obj);
        if (!validate.error) {
            API.post(`/kistpay-website-controller/submit-application`,obj).then((res)=>{
                console.log("handleSubmit",res)
                if(res.status === 200){
                    console.log("handleSubmit", obj)
                    setInit({
                        ...init,
                        "email": "",
                        "fullName": "",
                        "phoneNumber": "",
                        "query": ""
                    })
                    alert("Successfully Submitted");
                }else{
                    alert(res.data.message)
                }
            })
        } else {
            alert(validate.message)
        }
    }

    return (
        <Dialog
            open={open}
            keepMounted={false}
            fullWidth={true}
            maxWidth={maxWidth}
            sx={{'& .MuiDialog-paper': {borderRadius: '12px', width: `${width !== undefined && width}`}}}
            onClose={handleClose}
            aria-describedby="alert-dialog-slide-description"
        >

            <DialogTitle style={{
                padding: "24px",
                fontSize: "24px"
            }}>
                {title !== undefined && title}
            </DialogTitle>

            <IconButton
                style={{
                    position: "absolute",
                    right: "0"
                }}
                onClick={handleClose}
            >
                <CancelPresentationIcon
                    style={{
                        color: "red"
                    }}
                />
            </IconButton>

            <DialogContent style={{
                padding: "0 24px"
            }}>
                {
                    description && (
                        <DialogContentText style={{
                            margin: "0.5rem 0"
                        }}>
                            {description}
                        </DialogContentText>
                    )
                }
                <div className="contact-content">
                    <div className="frm">
                        <div className={`inp-grp ${f1 && "on"}`}>
                            <div className="ic">
                                {!f1 ? <img alt={""} src={f1g}/> : <img alt={""} src={f1b}/>}
                            </div>
                            <div className="inp">
                                <input type={"text"} value={init.fullName} name={"fullName"} onChange={handleChange}
                                       onFocus={() => handleActiveInput("f1", 1)}
                                       onBlur={() => handleActiveInput("f1", 2)} placeholder={"Enter your Full Name"}/>
                            </div>
                        </div>

                        <div className={`inp-grp ${f2 && "on"}`}>
                            <div className="ic">
                                {!f2 ? <img alt={""} src={f3g}/> : <img alt={""} src={f3b}/>}
                            </div>
                            <div className="inp">
                                <input type={"text"} value={init.phoneNumber} name={"phoneNumber"} onChange={handleChange}
                                       onFocus={() => handleActiveInput("f2", 1)}
                                       onBlur={() => handleActiveInput("f2", 2)}
                                       placeholder={"Enter your Phone Number"}/>
                            </div>
                        </div>

                        <div className={`inp-grp ${f3 && "on"}`}>
                            <div className="ic">
                                {!f3 ? <img alt={""} src={f6g}/> : <img alt={""} src={f6b}/>}
                            </div>
                            <div className="inp">
                                <input type={"text"} value={init.email} name={"email"} onChange={handleChange}
                                       onFocus={() => handleActiveInput("f3", 1)}
                                       onBlur={() => handleActiveInput("f3", 2)}
                                       placeholder={"Enter your Email Address"}/>
                            </div>
                        </div>

                        <div className={`inp-grp ${f4 && "on"}`}>
                            <div className="ic">
                                {!f4 ? <img alt={""} src={f8g}/> : <img alt={""} src={f8b}/>}
                            </div>
                            <div className="inp">
                                <input type={"text"} value={init.query} name={"query"} onChange={handleChange}
                                       onFocus={() => handleActiveInput("f4", 1)}
                                       onBlur={() => handleActiveInput("f4", 2)} placeholder={"Enter your Query"}/>
                            </div>
                        </div>

                    </div>
                </div>
            </DialogContent>
            {<DialogActions style={{
                padding: "24px"
            }}>
                <MuiButton variant="text" onClick={handleClose} label={"CANCEL"}/>
                <MuiButton variant="contained" color={"white"} onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    handleSubmit({
                        "email": init.email,
                        "fullName": init.fullName,
                        "phoneNumber": init.phoneNumber,
                        "query": init.query
                    })
                }}
                           label={actionLabel}/>
            </DialogActions>
            }
        </Dialog>
    );
};

export default ContactUsDialog;