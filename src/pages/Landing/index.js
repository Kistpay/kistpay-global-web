import React from 'react';
import "./index.css";
import Header from "../../SharedComponents/Header";
import SectionOne from "./Sections/One";
import SectionTwo from "./Sections/Two";
import SectionThree from "./Sections/Three";
import Partners from "./Sections/Partners";
import SectionFour from "./Sections/Four";
import SectionFive from "./Sections/Five";
import Footer from "../../SharedComponents/Footer";
import logo from "../../images/Kistpay_Logo.png";

const Landing = () => {
    return (
        <div className="main">
            {/*<div className="mini_logo">*/}
            {/*    <div>*/}
            {/*        <img alt={"#"} src={logo}/>*/}
            {/*    </div>*/}
            {/*    <div>*/}
            {/*        <img alt={"#"} src={logo}/>*/}
            {/*    </div>*/}
            {/*    <div>*/}
            {/*        <img alt={"#"} src={logo}/>*/}
            {/*    </div>*/}
            {/*    <div>*/}
            {/*        <img alt={"#"} src={logo}/>*/}
            {/*    </div>*/}
            {/*    <div>*/}
            {/*        <img alt={"#"} src={logo}/>*/}
            {/*    </div>*/}
            {/*</div>*/}
            <div className="mini_logo mini_logo_1">
                <div>
                    <img alt={"#"} src={logo}/>
                </div>
                <div>
                    <img alt={"#"} src={logo}/>
                </div>
                <div>
                    <img alt={"#"} src={logo}/>
                </div>
                <div>
                    <img alt={"#"} src={logo}/>
                </div>
                <div>
                    <img alt={"#"} src={logo}/>
                </div>
            </div>
            <Header/>
            <SectionOne />
            <SectionTwo/>
            <Partners/>
            <SectionThree/>
            <SectionFour/>
            <SectionFive/>
            <Footer/>
        </div>
    );
};

export default Landing;