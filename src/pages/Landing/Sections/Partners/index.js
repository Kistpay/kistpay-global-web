import React from 'react';
import s_1 from "../../../../images/sections/support/s_1.png";
import s_2 from "../../../../images/sections/support/s_2.png";
import s_3 from "../../../../images/sections/support/s_3.png";
import s_4 from "../../../../images/sections/support/s_4.png";
import s_5 from "../../../../images/sections/support/s_5.png";

const Partners = () => {
    return (
        <div className="section" style={{
            width:"100%"
        }}>
            <div className="lft">
                <div className="cont-box" style={{width:"100%"}}>
                    <div className="top" style={{justifyContent:"center"}}>
                        <div className="p">
                            <div className="p-bold-blue"> Our Trusted Partners </div>
                        </div>
                    </div>
                    <div className="btm">
                        <div className="list rw">
                            <div className="icons">
                                <div className="ic">
                                    <img alt={"#"} src={s_1}/>
                                </div>
                                <div className="ic">
                                    <img alt={"#"} src={s_2}/>
                                </div>
                                <div className="ic">
                                    <img alt={"#"} src={s_3}/>
                                </div>
                                <div className="ic">
                                    <img alt={"#"} src={s_4}/>
                                </div>
                                <div className="ic">
                                    <img alt={"#"} src={s_5}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Partners;