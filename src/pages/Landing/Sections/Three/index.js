import React from 'react';
import secThreeImg from "../../../../images/sections/three/group_3.png";
import checkIc from "../../../../images/sections/two/checkIc.png";
// import {ParallaxProvider,Parallax} from "react-scroll-parallax";

const SectionThree = () => {
    return (
        <div className="section" style={{
            backgroundColor: "white"
        }}>
            <div className="rt">
                <div className="img-hld">
                    <img alt={"#"} src={secThreeImg}/>
                    {/*<ParallaxProvider>*/}
                    {/*    <Parallax speed={-10}>*/}
                    {/*        <img alt={"#"} src={secThreeImg}/>*/}
                    {/*    </Parallax>*/}
                    {/*</ParallaxProvider>*/}
                </div>
            </div>
            <div className="lft">
                <div className="cont-box">
                    <div className="top">
                        <div className="p blk">
                            <div className="p-bold-blue">Transforming</div>
                            Smartphone <div className="p-bold">Retail Industry</div>
                        </div>
                    </div>
                    <div className="btm">
                        <div className="list">
                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-blue"> First Digital Smartphone Financing Platform
                                    Access tons of features on Kistpay App to manage your daily accounts
                                </div>
                            </div>

                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-blue"> Easy Application Process</div>
                            </div>

                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-blue"> Manage Daily Expense and Credit on the go
                                    View your Sales and Lending Dashboards
                                </div>
                            </div>

                        </div>
                        <div className="actions">
                            <div className="btn sec">
                                Get App For Free
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SectionThree;