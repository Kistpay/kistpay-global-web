import React from 'react';
import secTwoImg from "../../../../images/sections/two/group_2.png";
import checkIc from "../../../../images/sections/two/checkIc.png";
// import {Parallax, ParallaxProvider} from "react-scroll-parallax";

const SectionTwo = ({bg}) => {
    return (
        <div className="section bg_sec sec_2_mob" style={{
            backgroundColor: bg !== undefined ? bg : "white",
            // width:"100%"
        }}>
            <div className="lft">
                <div className="cont-box">
                    <div className="top">
                        <div className="p">
                            <div className="p-bold-green">Enabling lives</div>
                            with Smartphone Financing for the <div className="p-bold-blue">Un Banked</div>
                        </div>
                    </div>
                    <div className="btm">
                        <div className="list">
                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-blue mob"> Shariah Compliant</div>
                            </div>

                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-blue mob"> Interest-Free</div>
                            </div>

                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-blue mob"> Easy and affordable Installments</div>
                            </div>

                            <div className="li">
                                <div className="ic">
                                    <img alt={"#"} src={checkIc}/>
                                </div>
                                <div className="p-blue mob"> Low Up-front Cost</div>
                            </div>

                        </div>
                        {
                            bg === undefined && (
                                <div className="actions">
                                    <div className="btn sec">
                                        Get Your Phone
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
            </div>

                <div className="rt">
                    <div className="img-hld">
                        <img alt={"#"} src={secTwoImg}/>
                        {/*<ParallaxProvider>*/}
                        {/*<Parallax speed={-10}>*/}
                        {/*    <img alt={"#"} src={secTwoImg}/>*/}
                        {/*</Parallax>*/}
                        {/*</ParallaxProvider>*/}
                    </div>
                </div>
        </div>
    );
};

export default SectionTwo;