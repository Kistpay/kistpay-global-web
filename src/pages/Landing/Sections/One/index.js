import React from 'react';
import "./index.css";
import secOneImg from "../../../../images/sections/one/group_1.png";
import {Animate} from 'react-simple-animate';

const SectionOne = ({bg}) => {
    return (
        <div className={`section bg_sec sec_1_mob`} style={{
            backgroundColor: bg !== undefined && bg
        }}>
            <div className="lft">
                <div className="cont-box">
                    <div className="top">
                        <div className="p">
                            Looking for an <div className="p-bold-blue"> Easy Installment </div> plan to purchase a <div
                            className="p-bold"> Smartphone </div> today?
                        </div>
                    </div>
                    <div className="btm">
                        <div className="p">
                            <div className="p-bold-blue"> Kistpay</div>
                            is here to help!
                        </div>
                        <div className="actions">
                            <div className="btn sec">
                                For Customer
                            </div>
                            <div className="btn pri">
                                For Retailer
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="rt">
                <div className="img-hld">
                    <Animate
                        start={{ transform: 'translate(200px, 0)' }}
                        end={{ transform: 'translate(20px, 20px)' }}
                        play>
                    <img  alt={"#"} src={secOneImg}/>
                    </Animate>
                </div>
            </div>
        </div>
    );
};

export default SectionOne;