import React,{useState} from 'react';
import secFiveImg from "../../../../images/sections/five/sec_five.png";
import ContactUsDialog from "../../../ContactUs";

// import bg_five from "../../../../images/sections/five/bg.png";

const SectionFive = () => {
    const [openContactUs, setContactUs] = useState(false);
    return (
        <div className="section bg_sec sec_5_mob" style={{
            // backgroundColor: "white",
            // backgroundImage: `url(${bg_five})`,
            // backgroundSize: "cover",
            // backgroundRepeat: "no-repeat",
            padding:"3rem 6rem"
        }}>
            {openContactUs &&
                <ContactUsDialog
                    title={"Contact US"}
                    open={openContactUs}
                    handleClose={()=> setContactUs(false)}
                />
            }
            <div className="lft">
                <div className="cont-box">
                    <div className="top">
                        <div className="p">
                            Want to know more about <div className="p-bold"> Smartphone Financing? </div>
                        </div>
                    </div>
                    <div className="btm">
                        <div className="p">
                            <div className="p-bold-blue"> Reach out to Us</div>
                        </div>
                        <div className="actions">
                            <div className="btn pri" onClick={()=>{
                                setContactUs(true)
                            }}>
                                Contact Us
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="rt">
                <div className="img-hld">
                    <img alt={"#"} src={secFiveImg}/>
                </div>
            </div>
        </div>
    );
};

export default SectionFive;