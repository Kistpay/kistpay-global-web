import React, {useEffect, useState} from 'react';
import "./index.css";
import logo from "../../images/Kistpay_Logo.png";
import NavLink from "../NavLink"
import {Enums} from "../../utils/DefaultsEnums";
import {navigate} from "gatsby";
import {Helmet} from "react-helmet";
import MenuIcon from '@mui/icons-material/Menu';
import DefaultDrawer from "../Drawer/DefaultDrawer";
import $ from "jquery";

const KPHeader = () => {
    const [about_us, setAbout_us] = useState(false);
    const [openDrawer, setOpenDrawer] = useState(false);
    const [pathname, setPathName] = useState("");

    const handleClick = (ab) => {
        setAbout_us(!ab)
    }

    const handleClickLink = (path) => {
        setPathName(path)
    }

    return (
        <div className="kp-header-main">
            <div className="kp-head-holder">
                <Helmet>
                    <title>Kistpay Private Limited</title>
                </Helmet>
                <div className="lft">
                    <div className="logo">
                        <img alt={"#"} src={logo} onClick={() => {
                            navigate(Enums.ROUTES.DEFAULT)
                        }}/>
                    </div>
                </div>
                <div className="rt">
                    <div className="nav-holder">
                        <div className="anh">

                            <NavLink className={`link ${pathname === Enums.ROUTES.FOR_CUSTOMER && "on"}`}
                                     to={"#"} name={"Home"}
                                     onClick={() => handleClickLink(Enums.ROUTES.FOR_CUSTOMER)}/>
                            {/*<NavLink className={`link ${pathname === Enums.ROUTES.FOR_RETAILER && "on"}`}*/}
                            {/*         to={"#"} name={"Industries"}*/}
                            {/*         onClick={() => handleClickLink(Enums.ROUTES.FOR_RETAILER)}/>*/}
                            {/*<NavLink className={`link ${pathname === Enums.ROUTES.FOR_RETAILER && "on"}`}*/}
                            {/*         to={"#"} name={"Smartphones"}*/}
                            {/*         onClick={() => handleClickLink(Enums.ROUTES.FOR_RETAILER)}/>*/}
                            {/*<NavLink className={`link ${pathname === Enums.ROUTES.FOR_RETAILER && "on"}`}*/}
                            {/*         to={"#"} name={"Compare"}*/}
                            {/*         onClick={() => handleClickLink(Enums.ROUTES.FOR_RETAILER)}/>*/}
                            {/*<NavLink className={`link ${pathname === Enums.ROUTES.FOR_RETAILER && "on"}`}*/}
                            {/*         to={"#"} name={"Smartphone Insurance"}*/}
                            {/*         onClick={() => handleClickLink(Enums.ROUTES.FOR_RETAILER)}/>*/}
                            {/*<NavLink className={`link ${pathname === Enums.ROUTES.FOR_RETAILER && "on"}`}*/}
                            {/*         to={"#"} name={"Smartphone Anti-theft"}*/}
                            {/*         onClick={() => handleClickLink(Enums.ROUTES.FOR_RETAILER)}/>*/}
                            {/*<NavLink className={`link ${pathname === Enums.ROUTES.FOR_RETAILER && "on"}`}*/}
                            {/*         to={"#"} name={"Blogs"}*/}
                            {/*         onClick={() => handleClickLink(Enums.ROUTES.FOR_RETAILER)}/>*/}

                        </div>
                    </div>
                    {/*<div className="ic-btn">*/}
                    {/*    <MenuIcon onClick={() => {*/}
                    {/*        setOpenDrawer(true)*/}
                    {/*    }} style={{*/}
                    {/*        color: "#3465ea"*/}
                    {/*    }}/>*/}
                    {/*</div>*/}
                </div>
                {openDrawer &&
                    <DefaultDrawer
                        open={openDrawer}
                        handleClose={() => setOpenDrawer(false)}
                    />
                }
            </div>
        </div>
    );
};

export default KPHeader;