import React, {useEffect, useState} from 'react';
import {Slide} from 'react-slideshow-image'

const defaultState = {
    slides: []
}

const DefaultSlider = (props) => {
    let {data,arrows = false,indicators = true} = props;
    const [init, setInit] = useState(defaultState);

    useEffect(() => {
        if (data) {
            setInit({
                ...init,
                slides: data
            })
        }
    }, [])

    return (
        <div className="slides">
            <Slide easing="ease" arrows={arrows} indicators={indicators}>
                {
                    init.slides.map(({title, content}) => {
                        return (
                            <div className="each-slide">
                                <div className="title">
                                    <span>{title}</span>
                                </div>
                                <div className="content">
                                    <span>{content}</span>
                                </div>
                            </div>
                        )
                    })
                }
            </Slide>
        </div>
    );
};

export default DefaultSlider;