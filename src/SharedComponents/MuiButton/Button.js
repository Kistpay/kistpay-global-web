import React from 'react';
import {Button} from "@mui/material";
import {Enums} from "../../utils/DefaultsEnums";

const MuiButton = (props) => {
    let {label, onClick, variant = "contained", gaps, color} = props;
    let clrs = Enums.THEME;
    return (
        <Button
            style={{
                margin: gaps !== undefined ? gaps : "0 0 0 0.5rem",
                borderColor: `${color !== undefined ? color : clrs.PRIMARY_COLOR}`,
                color: `${color !== undefined ? color : clrs.PRIMARY_COLOR}`,
            }}
            variant={variant}
            onClick={onClick}
        >{label}
        </Button>
    );
};

export default MuiButton;