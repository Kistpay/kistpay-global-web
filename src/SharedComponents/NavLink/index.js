import React from 'react';
import {Link} from "gatsby";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const NavLink = ({className,to, name,onClick,toggleIcon}) => {
    return (
        toggleIcon !== undefined ?
            <React.Fragment>
                <Link className={className} to={to} onClick={onClick}>{name} {
                    toggleIcon ?
                        <ExpandLessIcon className="ic"/> :
                        <ExpandMoreIcon className="ic"/>
                }</Link>
            </React.Fragment>
            :
            <Link className={className} to={to} onClick={onClick}>{name}</Link>
    );
};

export default NavLink;