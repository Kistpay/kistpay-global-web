import React, {useState} from 'react';
import "./index.css";
import footLogo from "../../images/Kistpay_White_Logo.png";
import fb from "../../images/fb.png";
import tw from "../../images/tw.png";
import insta from "../../images/insta.png";
import {Enums} from "../../utils/DefaultsEnums";
import {Link} from "gatsby";

const Footer = () => {

    return (
        <div className="foot-holder">

            <div className="sec-content">
                <div className="divider">
                    <div className="clm">
                        <div className="logo">
                            <img alt={"#"} src={footLogo}/>
                        </div>
                        <div className="icons">
                            <img alt={"#"} src={fb}/>
                            <img alt={"#"} src={tw}/>
                            <img alt={"#"} src={insta}/>
                        </div>
                    </div>
                </div>
                <div className="divider">
                    <div className="clm">
                        <div className="txt">
                            <div className="head">
                                QUICK LINK
                            </div>
                            <div className="p">
                                <Link to={Enums.ROUTES.FOR_CUSTOMER} className="st">FOR CUSTOMER</Link>
                                <Link to={Enums.ROUTES.FOR_RETAILER} className="st">FOR RETAILER</Link>
                                {/*<div className="st">MARKETPLACE</div>*/}
                            </div>
                        </div>
                    </div>
                    <div className="clm">
                        <div className="txt">
                            <div className="head">
                                ABOUT US
                            </div>
                            <div className="p">
                                <Link to={Enums.ROUTES.COMPANY} className="st">COMPANY</Link>
                                {/*<div className="st">CAREER</div>*/}
                                {/*<div className="st">BLOGS</div>*/}
                            </div>
                        </div>
                    </div>
                    <div className="clm">
                        <div className="txt">
                            <div className="head">
                                LEGAL
                            </div>
                            <div className="p">
                                <Link to={Enums.ROUTES.TC} className="st">TERMS & CONDITION</Link>
                                <Link to={Enums.ROUTES.PP} className="st">PRIVACY POLICY</Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="divider">
                    <div className="clm">
                        <div className="p">
                            Kistpay (Pvt) Ltd is on a journey to enable the lives of masses across the world.
                        </div>
                    </div>
                </div>

            </div>
            <div className="cr">
                <span>2022 © Kistpay. All Rights Reserved.</span>
            </div>
        </div>
    );
};

export default Footer;