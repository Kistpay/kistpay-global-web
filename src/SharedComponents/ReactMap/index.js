import React, {useState} from 'react';
import "./index.css"
import GoogleMapReact from 'google-map-react';

const defaultState = {
    center: {
        lat: 24.8645,
        lng: 67.0252
    },
    zoom: 11
}

const ReactGoogleMap = (props) => {
    let {lng,lat,center} = props;
    const [init,setInit] = useState(defaultState);

    return (
        <div className="g-map">
            <GoogleMapReact
                bootstrapURLKeys={{ key: "AIzaSyALt2FvXHvguxotRe5ToKz6vsfI9mrhJLY"}}
                defaultCenter={init.center}
                defaultZoom={init.zoom}
            />
        </div>
    );
};

export default ReactGoogleMap;