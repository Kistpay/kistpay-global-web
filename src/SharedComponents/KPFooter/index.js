import React, {useState} from 'react';
import "./KPindex.css";
import footLogo from "../../images/Kistpay_White_Logo.png";
import fb from "../../images/fb.png";
import tw from "../../images/tw.png";
import insta from "../../images/insta.png";
import {Enums} from "../../utils/DefaultsEnums";
import {Link} from "gatsby";

const KPFooter = () => {

    return (
        <div className="kp-footer-main">
            <div className="kp-foot-holder">
                <div className="sec-content">
                    <div className="divider">
                        <div className="clm">
                            <div className="logo">
                                <img alt={"#"} src={footLogo}/>
                            </div>
                            <div className="p">
                                {/*Kistpay (Pvt) Ltd is on a journey to enable the lives of masses across the world.*/}
                                Kistpay (Pvt) Ltd is on a journey to enabling next billion users for digital connected life.
                            </div>
                        </div>
                    </div>
                    <div className="divider">
                        <div className="clm">
                            <div className="txt">
                                <div className="head">
                                    SOCIAL LINK
                                </div>
                            </div>
                            <div className="icons">
                                <img alt={"#"} src={fb}/>
                                <img alt={"#"} src={tw}/>
                                <img alt={"#"} src={insta}/>
                            </div>
                        </div>

                        {/*<div className="clm">*/}
                        {/*    <div className="txt">*/}
                        {/*        <div className="head">*/}
                        {/*            ABOUT US*/}
                        {/*        </div>*/}
                        {/*        <div className="p">*/}
                        {/*            <Link to={Enums.ROUTES.COMPANY} className="st">COMPANY</Link>*/}
                        {/*            /!*<div className="st">CAREER</div>*!/*/}
                        {/*            /!*<div className="st">BLOGS</div>*!/*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        {/*<div className="clm">*/}
                        {/*    <div className="txt">*/}
                        {/*        <div className="head">*/}
                        {/*            LEGAL*/}
                        {/*        </div>*/}
                        {/*        <div className="p">*/}
                        {/*            <Link to={Enums.ROUTES.TC} className="st">TERMS & CONDITION</Link>*/}
                        {/*            <Link to={Enums.ROUTES.PP} className="st">PRIVACY POLICY</Link>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                    </div>
                    <div className="divider">
                        <div className="clm">
                            <div className="txt">
                                <div className="head">
                                    Quick Link
                                </div>
                                <div className="p">
                                    <Link to={"#"} className="st">Home</Link>
                                    {/*<Link to={"#"} className="st">Industries</Link>*/}
                                    {/*<Link to={"#"} className="st">Smartphones</Link>*/}
                                    {/*<Link to={"#"} className="st">Compare</Link>*/}
                                    {/*<Link to={"#"} className="st">Smartphone Insurance</Link>*/}
                                    {/*<Link to={"#"} className="st">Smartphone Anti-theft</Link>*/}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="divider">
                        <div className="clm">
                            <div className="txt">
                                <div className="head">
                                    Subscribe Us
                                </div>
                                <div className="subs-fields">
                                    <div className="hld">
                                        <div className="inp-grp">
                                            <div className="inp">
                                                <input type="text" placeholder={"Email"}
                                                       onChange={(fl) => console.log("onChange >>", fl)}/>
                                            </div>
                                        </div>
                                        <div className="btn">
                                            Submit
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div className="cr">
                <span>2022 © Kistpay. All Rights Reserved.</span>
            </div>
        </div>
    );
};

export default KPFooter;