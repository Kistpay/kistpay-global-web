import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import HomeIcon from '@mui/icons-material/Home';
import PersonIcon from '@mui/icons-material/Person';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import BusinessIcon from '@mui/icons-material/Business';
import BusinessCenterIcon from '@mui/icons-material/BusinessCenter';
import {navigate} from "gatsby";
import {Enums} from "../../utils/DefaultsEnums";

export default function DefaultDrawer({handleClose, open}) {
    console.log("DefaultDrawer", open)

    const list = () => (
        <Box
            sx={{width: 250}}
            role="presentation"
            onClick={() => handleClose(false)}
            onKeyDown={() => handleClose(false)}
        >
            <List>
                <ListItem button onClick={()=>{
                    navigate(Enums.ROUTES.DEFAULT)
                }}>
                    <ListItemIcon>
                        <HomeIcon style={{
                            color: "#3465ea"
                        }}/>
                    </ListItemIcon>
                    <ListItemText style={{
                        color:"#3465ea"
                    }} primary={"Home"}/>
                </ListItem>
                <Divider/>
                <ListItem button onClick={()=>{
                    navigate(Enums.ROUTES.FOR_CUSTOMER)
                }}>
                    <ListItemIcon>
                        <PersonIcon style={{
                            color: "#3465ea"
                        }}/>
                    </ListItemIcon>
                    <ListItemText style={{
                        color:"#3465ea"
                    }} primary={"For Customer"}/>
                </ListItem>
                <Divider/>
                <ListItem button onClick={()=>{
                    navigate(Enums.ROUTES.FOR_RETAILER)
                }}>
                    <ListItemIcon>
                        <AccountBoxIcon style={{
                            color: "#3465ea"
                        }}/>
                    </ListItemIcon>
                    <ListItemText style={{
                        color:"#3465ea"
                    }} primary={"For Retailer"}/>
                </ListItem>
                <Divider/>
                <ListItem button onClick={()=>{
                    navigate(Enums.ROUTES.COMPANY)
                }}>
                    <ListItemIcon>
                        <BusinessIcon
                            style={{
                                color: "#3465ea"
                            }}
                        />
                    </ListItemIcon>
                    <ListItemText style={{
                        color:"#3465ea"
                    }} primary={"Company"}/>
                </ListItem>
                <Divider/>
                {/*<ListItem button onClick={()=>{*/}
                {/*    navigate(Enums.ROUTES.CAREER)*/}
                {/*}}>*/}
                {/*    <ListItemIcon>*/}
                {/*        <BusinessCenterIcon style={{*/}
                {/*            color: "#3465ea"*/}
                {/*        }}/>*/}
                {/*    </ListItemIcon>*/}
                {/*    <ListItemText style={{*/}
                {/*        color:"#3465ea"*/}
                {/*    }} primary={"Career"}/>*/}
                {/*</ListItem>*/}
                <Divider/>
            </List>
        </Box>
    );

    return (
        <Drawer
            anchor={"right"}
            open={open}
            onClose={() => handleClose(false)}
        >
            {list()}
        </Drawer>

    );
}