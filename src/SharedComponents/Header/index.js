import React, {useEffect, useState} from 'react';
import "./index.css";
import logo from "../../images/Kistpay_Logo.png";
import NavLink from "../NavLink"
import {Enums} from "../../utils/DefaultsEnums";
import {navigate} from "gatsby";
import {Helmet} from "react-helmet";
import MenuIcon from '@mui/icons-material/Menu';
import DefaultDrawer from "../Drawer/DefaultDrawer";

const Header = () => {
    const [about_us, setAbout_us] = useState(false);
    const [openDrawer, setOpenDrawer] = useState(false);
    const [pathname, setPathName] = useState("");

    const handleClick = (ab) => {
        setAbout_us(!ab)
    }

    const handleClickLink = (path) => {
        setPathName(path)
    }

    return (
        <div className="head-holder">
            <Helmet>
                <title>Kistpay Private Limited</title>
            </Helmet>
            <div className="lft">
                <div className="logo">
                    <img alt={"#"} src={logo} onClick={() => {
                        navigate(Enums.ROUTES.DEFAULT)
                    }}/>
                </div>
            </div>
            <div className="rt">
                <div className="nav-holder">
                    <div className="anh">
                        <NavLink className={`link ${pathname === Enums.ROUTES.FOR_CUSTOMER && "on"}`}
                                 to={Enums.ROUTES.FOR_CUSTOMER} name={"For Customer"} onClick={()=> handleClickLink(Enums.ROUTES.FOR_CUSTOMER)}/>
                        <NavLink className={`link ${pathname === Enums.ROUTES.FOR_RETAILER && "on"}`}
                                 to={Enums.ROUTES.FOR_RETAILER} name={"For Retailer"} onClick={()=> handleClickLink(Enums.ROUTES.FOR_RETAILER)}/>
                        {/*<NavLink className="link" to={Enums.ROUTES.DEFAULT} name={"Market Place"}/>*/}
                        <NavLink className={`link dr ${pathname === Enums.ROUTES.COMPANY && "on"}`}
                                 to={"#"} name={"About Us"}
                                 toggleIcon={about_us}
                                 onClick={() => {
                                     handleClick(about_us)
                                     handleClickLink(Enums.ROUTES.COMPANY)
                                 }} />
                        {
                            about_us &&
                            <div className="link-drop">
                                <NavLink className="ch-link" to={Enums.ROUTES.COMPANY} name={"Company"}/>
                                {/*<NavLink className="ch-link" to={Enums.ROUTES.CAREER} name={"Career"}/>*/}
                                {/*<NavLink className="ch-link" to={Enums.ROUTES.DEFAULT} name={"Blog"}/>*/}
                            </div>
                        }
                        <a href={"#"} className="link-btn" target={"_blank"}>Get Financing</a>
                        {/*https://kistpay.com/*/}
                        {/*<NavLink onClick={()=>{*/}
                        {/*    // window.open("https://kistpay.com/","_blank");*/}
                        {/*    navigate("https://kistpay.com/")*/}

                        {/*}} className="link-btn" to={Enums.ROUTES.DEFAULT} name={"Get Financing"}/>*/}
                    </div>
                </div>
                <div className="ic-btn">
                    <MenuIcon onClick={() => {
                        setOpenDrawer(true)
                    }} style={{
                        color: "#3465ea"
                    }}/>
                </div>
            </div>
            {openDrawer &&
                <DefaultDrawer
                    open={openDrawer}
                    handleClose={() => setOpenDrawer(false)}
                />
            }
        </div>
    );
};

export default Header;